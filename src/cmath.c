#include <stdlib.h>
#include <math.h>
#include "cmath.h"
#include "value.h"
#include "panic.h"

#define INIT_FUNC(name, impl, ...) \
    { \
        EhValue *paramv[] = { __VA_ARGS__ }; \
        size_t paramc = sizeof(paramv) / sizeof(EhValue *); \
        EhFunction *implv[] = { (EhFunction *) new_function(rt, NULL, impl, (EhHtrie *) scope, paramc, paramv, 0, NULL), }; \
        EhValue *atom = ehatom_from_cstr(rt, NULL, name); \
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv); \
        math = ehhtrie_assoc(rt, math, atom, closure); \
    }

void init_lib_cmath(EhRt *rt) {
    EhValue *scope = ehrt_peek(rt, 0);
    EhHtrie *math = new_htrie(rt);
    INIT_FUNC("sin", cmath_sin, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("tan", cmath_tan, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("acos", cmath_acos, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("asin", cmath_asin, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("atan", cmath_atan, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("atan2", cmath_atan2, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("cosh", cmath_cosh, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("sinh", cmath_sinh, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("tanh", cmath_tanh, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("acosh", cmath_acosh, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("asinh", cmath_asinh, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("atanh", cmath_atanh, new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("exp", cmath_exp, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("frexp", cmath_frexp, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("ldexp", cmath_ldexp, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("log", cmath_log, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("log10", cmath_log10, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("modf", cmath_modf, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("exp2", cmath_exp2, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("expm1", cmath_expm1, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("ilogb", cmath_ilogb, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("log1p", cmath_log1p, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("log2", cmath_log2, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("logb", cmath_logb, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("scalbn", cmath_scalbn, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("scalbln", cmath_scalbln, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_INT, NULL));
    
    INIT_FUNC("pow", cmath_pow, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("sqrt", cmath_sqrt, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("cbrt", cmath_cbrt, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("hypot", cmath_hypot, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("erf", cmath_erf, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("erfc", cmath_erfc, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("tgamma", cmath_tgamma, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("lgamma", cmath_lgamma, new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("ceil", cmath_ceil, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("floor", cmath_floor, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("fmod", cmath_fmod, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("trunc", cmath_trunc, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("round", cmath_round, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("lround", cmath_lround, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("rint", cmath_rint, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("lrint", cmath_lrint, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("nearbyint", cmath_nearbyint, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("remainder", cmath_remainder, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("remquo", cmath_remquo, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("copysign", cmath_copysign, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("nan", cmath_nan);
    INIT_FUNC("nextafter", cmath_nextafter, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("nexttoward", cmath_nexttoward, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("fdim", cmath_fdim, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("fmax", cmath_fmax, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("fmin", cmath_fmin, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("fabs", cmath_fabs, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("abs", cmath_abs, new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("fma", cmath_fma, new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL));
    
    INIT_FUNC("fpclassify", cmath_fpclassify, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("isfinite", cmath_isfinite, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("isinf", cmath_isinf, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("isnan", cmath_isnan, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("isnormal", cmath_isnormal, new_bind(rt, NULL, EH_REAL, NULL));
    INIT_FUNC("signbit", cmath_signbit, new_bind(rt, NULL, EH_REAL, NULL));

    EhHtrie *env = ehhtrie_assoc(rt, (EhHtrie *) scope, ehatom_from_cstr(rt, NULL, "cmath"), (EhValue *) math);
    rt->stack[0] = (EhValue *) env;
}

#undef INIT_FUNC

EhValue *cmath_sin(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, sin(((EhReal *) argv[0])->value));
}

EhValue *cmath_tan(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, tan(((EhReal *) argv[0])->value));
}

EhValue *cmath_acos(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, acos(((EhReal *) argv[0])->value));
}

EhValue *cmath_asin(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, asin(((EhReal *) argv[0])->value));
}

EhValue *cmath_atan(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, atan(((EhReal *) argv[0])->value));
}

EhValue *cmath_atan2(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, atan2(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_cosh(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, cosh(((EhReal *) argv[0])->value));
}

EhValue *cmath_sinh(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, sinh(((EhReal *) argv[0])->value));
}

EhValue *cmath_tanh(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, tanh(((EhReal *) argv[0])->value));
}

EhValue *cmath_acosh(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, acosh(((EhReal *) argv[0])->value));
}

EhValue *cmath_asinh(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, asinh(((EhReal *) argv[0])->value));
}

EhValue *cmath_atanh(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, atanh(((EhReal *) argv[0])->value));
}


EhValue *cmath_exp(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, exp(((EhReal *) argv[0])->value));
}

EhValue *cmath_frexp(EhRt *rt, EhValue **data, EhValue **argv) {
    int exp;
    double fr = frexp(((EhReal *) argv[0])->value, &exp);
    ehgc_disable(rt, &rt->gc);
    EhValue *result = new_list_cons(rt, NULL, new_real(rt, NULL, fr),
                        new_list_cons(rt, NULL, new_int(rt, NULL, exp),
                            new_list_empty(rt, NULL)));
    ehgc_enable(rt, &rt->gc);
    return result;
}

EhValue *cmath_ldexp(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, ldexp(((EhReal *) argv[0])->value, ((EhInt *) argv[1])->value));
}

EhValue *cmath_log(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, log(((EhReal *) argv[0])->value));
}

EhValue *cmath_log10(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, log10(((EhReal *) argv[0])->value));
}

EhValue *cmath_modf(EhRt *rt, EhValue **data, EhValue **argv) {
    double intpart;
    double fr = modf(((EhReal *) argv[0])->value, &intpart);
    ehgc_disable(rt, &rt->gc);
    EhValue *result = new_list_cons(rt, NULL, new_real(rt, NULL, fr),
                        new_list_cons(rt, NULL, new_real(rt, NULL, intpart),
                            new_list_empty(rt, NULL)));
    ehgc_enable(rt, &rt->gc);
    return result;
}

EhValue *cmath_exp2(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, exp2(((EhReal *) argv[0])->value));
}

EhValue *cmath_expm1(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, expm1(((EhReal *) argv[0])->value));
}

EhValue *cmath_ilogb(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, ilogb(((EhReal *) argv[0])->value));
}

EhValue *cmath_log1p(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, log1p(((EhReal *) argv[0])->value));
}

EhValue *cmath_log2(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, log2(((EhReal *) argv[0])->value));
}

EhValue *cmath_logb(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, logb(((EhReal *) argv[0])->value));
}

EhValue *cmath_scalbn(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, scalbn(((EhReal *) argv[0])->value, ((EhInt *) argv[1])->value));
}

EhValue *cmath_scalbln(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, scalbln(((EhReal *) argv[0])->value, ((EhInt *) argv[1])->value));
}

EhValue *cmath_pow(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, pow(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_sqrt(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, sqrt(((EhReal *) argv[0])->value));
}

EhValue *cmath_cbrt(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, cbrt(((EhReal *) argv[0])->value));
}

EhValue *cmath_hypot(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, hypot(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_erf(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, erf(((EhReal *) argv[0])->value));
}

EhValue *cmath_erfc(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, erfc(((EhReal *) argv[0])->value));
}

EhValue *cmath_tgamma(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, tgamma(((EhReal *) argv[0])->value));
}

EhValue *cmath_lgamma(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, lgamma(((EhReal *) argv[0])->value));
}

EhValue *cmath_ceil(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, ceil(((EhReal *) argv[0])->value));
}

EhValue *cmath_floor(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, floor(((EhReal *) argv[0])->value));
}

EhValue *cmath_fmod(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, fmod(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_trunc(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, trunc(((EhReal *) argv[0])->value));
}

EhValue *cmath_round(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, round(((EhReal *) argv[0])->value));
}

EhValue *cmath_lround(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, lround(((EhReal *) argv[0])->value));
}

EhValue *cmath_rint(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, rint(((EhReal *) argv[0])->value));
}

EhValue *cmath_lrint(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, lrint(((EhReal *) argv[0])->value));
}

EhValue *cmath_nearbyint(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, nearbyint(((EhReal *) argv[0])->value));
}

EhValue *cmath_remainder(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, remainder(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_remquo(EhRt *rt, EhValue **data, EhValue **argv) {
    int quot;
    double fr = remquo(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value, &quot);
    ehgc_disable(rt, &rt->gc);
    EhValue *result = new_list_cons(rt, NULL, new_real(rt, NULL, fr),
                        new_list_cons(rt, NULL, new_int(rt, NULL, quot),
                            new_list_empty(rt, NULL)));
    ehgc_enable(rt, &rt->gc);
    return result;
}

EhValue *cmath_copysign(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, copysign(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_nan(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, nan(""));
}

EhValue *cmath_nextafter(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, nextafter(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_nexttoward(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, nexttoward(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_fdim(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, fdim(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_fmax(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, fmax(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}
EhValue *cmath_fmin(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, fmin(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value));
}

EhValue *cmath_fabs(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, fabs(((EhReal *) argv[0])->value));
}

EhValue *cmath_abs(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, labs(((EhInt *) argv[0])->value));
}

EhValue *cmath_fma(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, fma(((EhReal *) argv[0])->value, ((EhReal *) argv[1])->value, ((EhReal *) argv[2])->value));
}

EhValue *cmath_fpclassify(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fpclassify(((EhReal *) argv[0])->value));
}

EhValue *cmath_isfinite(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_bool(rt, NULL, isfinite(((EhReal *) argv[0])->value));
}

EhValue *cmath_isinf(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_bool(rt, NULL, isinf(((EhReal *) argv[0])->value));
}

EhValue *cmath_isnan(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_bool(rt, NULL, isnan(((EhReal *) argv[0])->value));
}

EhValue *cmath_isnormal(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_bool(rt, NULL, isnormal(((EhReal *) argv[0])->value));
}

EhValue *cmath_signbit(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, signbit(((EhReal *) argv[0])->value));
}
