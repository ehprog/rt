#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "sys.h"
#include "value.h"
#include "panic.h"

#define INIT_FUNC(name, impl, ...) \
    { \
        EhValue *paramv[] = { __VA_ARGS__ }; \
        size_t paramc = sizeof(paramv) / sizeof(EhValue *); \
        EhFunction *implv[] = { (EhFunction *) new_function(rt, NULL, impl, (EhHtrie *) scope, paramc, paramv, 0, NULL), }; \
        EhValue *atom = ehatom_from_cstr(rt, NULL, name); \
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv); \
        sys = ehhtrie_assoc(rt, sys, atom, closure); \
    }

void init_lib_sys(EhRt *rt) {
    EhValue *scope = ehrt_peek(rt, 0);
    EhHtrie *sys = new_htrie(rt);

    /* ffi */
    INIT_FUNC("null", sys_null, new_nil(rt, NULL));
    INIT_FUNC("ptr-offset", sys_null, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("ptr-diff", sys_null, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("cstr->string", sys_cstr_to_string, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("string->cstr", sys_string_to_cstr, new_bind(rt, NULL, EH_LIST, NULL));

    /* stdlib.h */
    INIT_FUNC("exit", sys_exit, new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("abort", sys_abort, new_nil(rt, NULL));
    INIT_FUNC("system", sys_system, new_bind(rt, NULL, EH_PTR, NULL));

    INIT_FUNC("rand", sys_rand, new_nil(rt, NULL));
    INIT_FUNC("srand", sys_srand, new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("randmax", sys_randmax, new_nil(rt, NULL));

    INIT_FUNC("malloc", sys_malloc, new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("calloc", sys_calloc, new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("free", sys_free, new_bind(rt, NULL, EH_PTR, NULL));

    /* stdio.h */
    INIT_FUNC("stdin", sys_stdin, new_nil(rt, NULL));
    INIT_FUNC("stdout", sys_stdout, new_nil(rt, NULL));
    INIT_FUNC("stderr", sys_stderr, new_nil(rt, NULL));

    INIT_FUNC("fopen", sys_fopen, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fclose", sys_fclose, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fflush", sys_fflush, new_bind(rt, NULL, EH_PTR, NULL));

    INIT_FUNC("fread", sys_fread, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fgetc", sys_fgetc, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fgets", sys_fgets, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fwrite", sys_fwrite, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fputc", sys_fputc, new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fputs", sys_fputs, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_PTR, NULL));

    INIT_FUNC("feof", sys_feof, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("ferror", sys_ferror, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("fseek", sys_fseek, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("ftell", sys_ftell, new_bind(rt, NULL, EH_PTR, NULL));

    INIT_FUNC("remove", sys_remove, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("rename", sys_rename, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("seek-end", sys_seek_end, new_nil(rt, NULL));
    INIT_FUNC("seek-cur", sys_seek_cur, new_nil(rt, NULL));
    INIT_FUNC("seek-set", sys_seek_set, new_nil(rt, NULL));

    /* string.h */
    INIT_FUNC("memchr", sys_memchr, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("memcmp", sys_memcmp, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("memcpy", sys_memcpy, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("memmove", sys_memmove, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL));
    INIT_FUNC("memset", sys_memset, new_bind(rt, NULL, EH_PTR, NULL), new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL));

    /* time.h */
    INIT_FUNC("clock", sys_clock, new_nil(rt, NULL));
    INIT_FUNC("clocks-per-sec", sys_clocks_per_sec, new_nil(rt, NULL));
    INIT_FUNC("time", sys_time, new_nil(rt, NULL));
    INIT_FUNC("localtime", sys_localtime, new_nil(rt, NULL));
    INIT_FUNC("gmtime", sys_gmtime, new_nil(rt, NULL));

    INIT_FUNC("tm-sec", sys_tm_sec, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-min", sys_tm_min, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-hour", sys_tm_hour, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-mday", sys_tm_mday, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-mon", sys_tm_mon, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-year", sys_tm_year, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-wday", sys_tm_wday, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-yday", sys_tm_yday, new_bind(rt, NULL, EH_PTR, NULL));
    INIT_FUNC("tm-isdst", sys_tm_isdst, new_bind(rt, NULL, EH_PTR, NULL));

#undef INIT_FUNC

    EhHtrie *env = ehhtrie_assoc(rt, (EhHtrie *) scope, ehatom_from_cstr(rt, NULL, "sys"), (EhValue *) sys);
    rt->stack[0] = (EhValue *) env;
}

/* ffi */
EhValue *sys_null(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, NULL);
}

EhValue *sys_ptr_offset(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, (void *) ((size_t) ((EhPtr *) argv[0])->ptr + (ptrdiff_t) ((EhInt *) argv[1])->value));
}

EhValue *sys_ptr_diff(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, (ptrdiff_t) ((EhPtr *) argv[0])->ptr - (ptrdiff_t) ((EhPtr *) argv[1])->ptr);
}

EhValue *sys_cstr_to_string(EhRt *rt, EhValue **data, EhValue **argv) {
    char *str = ((EhPtr *) argv[0])->ptr;
    return ehstr_from_pstr(rt, NULL, strlen(str), str);
}

EhValue *sys_string_to_cstr(EhRt *rt, EhValue **data, EhValue **argv) {
    char *str;
    size_t len = 0;
    EhList *list = (EhList *) argv[0];
    while (list->variant == EH_LIST_CONS) {
        ++len;
        list = ((EhListCons *) list)->tail;
    }
    str = malloc(len + 1);
    list = (EhList *) argv[0];
    size_t i = 0;
    while (list->variant == EH_LIST_CONS) {
        EhValue *value = ((EhListCons *) list)->head;
        assert_eq(rt, value->type, EH_CHAR, "list is not a string");
        str[i] = ((EhChar *) value)->value;
        ++i;
        list = ((EhListCons *) list)->tail;
    }
    str[i] = 0;
    return new_ptr(rt, NULL, str);
}

/* stdlib.h */
EhValue *sys_exit(EhRt *rt, EhValue **data, EhValue **argv) {
    exit(((EhInt *) argv[0])->value);
}

EhValue *sys_abort(EhRt *rt, EhValue **data, EhValue **argv) {
    abort();
}

EhValue *sys_system(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, system(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_rand(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, rand());
}

EhValue *sys_srand(EhRt *rt, EhValue **data, EhValue **argv) {
    srand(((EhInt *) argv[0])->value);
    return new_nil(rt, NULL);
}

EhValue *sys_randmax(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, RAND_MAX);
}

EhValue *sys_malloc(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, malloc(((EhInt *) argv[0])->value));
}

EhValue *sys_calloc(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, calloc(((EhInt *) argv[0])->value, ((EhInt *) argv[0])->value));
}

EhValue *sys_free(EhRt *rt, EhValue **data, EhValue **argv) {
    free(((EhPtr *) argv[0])->ptr);
    return new_nil(rt, NULL);
}

/* stdio.h */
EhValue *sys_stdin(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, stdin);
}

EhValue *sys_stdout(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, stdout);
}

EhValue *sys_stderr(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, stderr);
}

EhValue *sys_fopen(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, fopen(((EhPtr *) argv[0])->ptr, ((EhPtr *) argv[1])->ptr));
}

EhValue *sys_fclose(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fclose(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_fflush(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fflush(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_fread(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fread(((EhPtr *) argv[0])->ptr, ((EhInt *) argv[1])->value, ((EhInt *) argv[2])->value, ((EhPtr *) argv[3])->ptr));
}

EhValue *sys_fgetc(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fgetc(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_fgets(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, fgets(((EhPtr *) argv[0])->ptr, ((EhInt *) argv[1])->value, ((EhPtr *) argv[2])->ptr));
}

EhValue *sys_fwrite(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fwrite(((EhPtr *) argv[0])->ptr, ((EhInt *) argv[1])->value, ((EhInt *) argv[2])->value, ((EhPtr *) argv[3])->ptr));
}

EhValue *sys_fputc(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fputc(((EhInt *) argv[0])->value, ((EhPtr *) argv[1])->ptr));
}

EhValue *sys_fputs(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fputs(((EhPtr *) argv[0])->ptr, ((EhPtr *) argv[1])->ptr));
}

EhValue *sys_feof(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, feof(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_ferror(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ferror(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_fseek(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, fseek(((EhPtr *) argv[0])->ptr, ((EhInt *) argv[1])->value, ((EhInt *) argv[2])->value));
}

EhValue *sys_ftell(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ftell(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_remove(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, remove(((EhPtr *) argv[0])->ptr));
}

EhValue *sys_rename(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, rename(((EhPtr *) argv[0])->ptr, ((EhPtr *) argv[1])->ptr));
}

EhValue *sys_seek_end(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, SEEK_END);
}

EhValue *sys_seek_cur(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, SEEK_CUR);
}

EhValue *sys_seek_set(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, SEEK_SET);
}

/* string.h */
EhValue *sys_memchr(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, memchr(((EhPtr *) argv[0])->ptr, ((EhInt *) argv[1])->value, ((EhInt *) argv[2])->value));
}

EhValue *sys_memcmp(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, memcmp(((EhPtr *) argv[0])->ptr, ((EhPtr *) argv[1])->ptr, ((EhInt *) argv[2])->value));
}

EhValue *sys_memcpy(EhRt *rt, EhValue **data, EhValue **argv) {
    memcpy(((EhPtr *) argv[0])->ptr, ((EhPtr *) argv[1])->ptr, ((EhInt *) argv[2])->value);
    return new_nil(rt, NULL);
}

EhValue *sys_memmove(EhRt *rt, EhValue **data, EhValue **argv) {
    memmove(((EhPtr *) argv[0])->ptr, ((EhPtr *) argv[1])->ptr, ((EhInt *) argv[2])->value);
    return new_nil(rt, NULL);
}

EhValue *sys_memset(EhRt *rt, EhValue **data, EhValue **argv) {
    memset(((EhPtr *) argv[0])->ptr, ((EhInt *) argv[1])->value, ((EhInt *) argv[2])->value);
    return new_nil(rt, NULL);
}

/* time.h */
EhValue *sys_clock(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, clock());
}

EhValue *sys_clocks_per_sec(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, CLOCKS_PER_SEC);
}

EhValue *sys_time(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, time(NULL));
}

EhValue *sys_localtime(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, localtime(NULL));
}

EhValue *sys_gmtime(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_ptr(rt, NULL, gmtime(NULL));
}

EhValue *sys_tm_sec(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_sec);
}

EhValue *sys_tm_min(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_min);
}

EhValue *sys_tm_hour(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_hour);
}

EhValue *sys_tm_mday(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_mday);
}

EhValue *sys_tm_mon(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_mon);
}

EhValue *sys_tm_year(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_year);
}

EhValue *sys_tm_wday(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_wday);
}

EhValue *sys_tm_yday(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_yday);
}

EhValue *sys_tm_isdst(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((struct tm *) ((EhPtr *) argv[0])->ptr)->tm_isdst);
}

#undef INIT_FUNC
