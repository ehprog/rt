#include "rt.h"
#include "value.h"
#include "ratio.h"
#include "panic.h"

static unsigned long gcd(unsigned long p, unsigned long q) {
    if (p == 0) {
        return q;
    }
    if (q == 0) {
        return p;
    }

    if ((p & 1) == 0 && (q & 1) == 0) {
        return 2 * gcd(p >> 1, q >> 1);
    } else if ((p & 1) == 0 && (q & 1) == 1) {
        return gcd(p >> 1, q);
    } else if ((p & 1) == 1 && (q & 1) == 0) {
        return gcd(p, q >> 1);
    } else if (p >= q) {
        return gcd((p - q) >> 1, q);
    } else {
        return gcd((q - p) >> 1, p);
    }
}

static unsigned long lcm(unsigned long q1, unsigned long q2) {
    return q1 * q2 / gcd(q1, q2);
}

EhValue *new_ratio(EhRt *rt, EhRatio *ratio, enum EhRatioSign sign, unsigned long p, unsigned long q) {
    if (!ratio) {
        ratio = (EhRatio *) ehgc_alloc(rt, &rt->gc, EH_RATIO, sizeof(EhRatio));
    }

    unsigned long g = gcd(p, q);
    p /= g;
    q /= g;
    ratio->sign = sign;
    ratio->p = p;
    ratio->q = q;

    return (EhValue *) ratio;
}

EhValue *ehratio_neg(EhRt *rt, EhRatio *lhs) {
    return new_ratio(rt, NULL, !lhs->sign, lhs->p, lhs->q);
}

EhValue *ehratio_add(EhRt *rt, EhRatio *lhs, EhRatio *rhs) {
    unsigned long q = lcm(lhs->q, rhs->q);
    unsigned long l1 = q / lhs->q;
    unsigned long l2 = q / rhs->q;
    enum EhRatioSign sign;
    int s = lhs->sign ^ rhs->sign;
    unsigned long a = lhs->p * l1;
    unsigned long b = rhs->p * l2;
    unsigned long p;
    if (s) {
        sign = b > a;
        p = a - b;
        if (sign) {
            p = -p;
        }
    } else {
        sign = a >= b;
        p = a + b;
        if (!sign) {
            p = -p;
        }
    }
    return new_ratio(rt, NULL, sign, p, q);
}

EhValue *ehratio_sub(EhRt *rt, EhRatio *lhs, EhRatio *rhs) {
    EhRatio rhs_;
    new_ratio(rt, &rhs_, !rhs->sign, rhs->p, rhs->q);
    return ehratio_add(rt, lhs, &rhs_);
}

EhValue *ehratio_mul(EhRt *rt, EhRatio *lhs, EhRatio *rhs) {
    unsigned long p = lhs->p * rhs->p;
    unsigned long q = lhs->q * rhs->q;
    return new_ratio(rt, NULL, lhs->sign ^ rhs->sign, p, q);
}

EhValue *ehratio_div(EhRt *rt, EhRatio *lhs, EhRatio *rhs) {
    unsigned long p = lhs->p * rhs->q;
    unsigned long q = lhs->q * rhs->p;
    return new_ratio(rt, NULL, lhs->sign ^ rhs->sign, p, q);
}

int ehratio_eq(EhRt *rt, const EhRatio *a, const EhRatio *b) {
    return a->sign == b->sign && a->p == b->p && a->q == b->q;
}

int ehratio_ne(EhRt *rt, const EhRatio *a, const EhRatio *b) {
    return !ehratio_eq(rt, a, b);
}

int ehratio_lt(EhRt *rt, const EhRatio *a, const EhRatio *b) {
    unsigned long p1 = -a->sign * a->p;
    unsigned long p2 = -b->sign * b->p;
    unsigned long q1 = a->q;
    unsigned long q2 = b->q;
    return p1*q2 < p2*q1;
}

int ehratio_gt(EhRt *rt, const EhRatio *a, const EhRatio *b) {
    unsigned long p1 = -a->sign * a->p;
    unsigned long p2 = -b->sign * b->p;
    unsigned long q1 = a->q;
    unsigned long q2 = b->q;
    return p1*q2 > p2*q1;
}

int ehratio_le(EhRt *rt, const EhRatio *a, const EhRatio *b) {
    unsigned long p1 = -a->sign * a->p;
    unsigned long p2 = -b->sign * b->p;
    unsigned long q1 = a->q;
    unsigned long q2 = b->q;
    return p1*q2 <= p2*q1;
}

int ehratio_ge(EhRt *rt, const EhRatio *a, const EhRatio *b) {
    unsigned long p1 = -a->sign * a->p;
    unsigned long p2 = -b->sign * b->p;
    unsigned long q1 = a->q;
    unsigned long q2 = b->q;
    return p1*q2 >= p2*q1;
}

void ehratio_hash(EhRt *rt, EhHasher *state, const EhRatio *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_u64(state, a->sign);
    ehhasher_update_u64(state, a->p);
    ehhasher_update_u64(state, a->q);
}

EhValue *ehratio_repr(EhRt *rt, EhRatio *i) {
    EhValue *list = new_list_empty(rt, NULL);
    long q = i->q;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + q % 10), list);
        q /= 10;
    } while (q);
    list = new_list_cons(rt, NULL, new_char(rt, NULL, '/'), list);
    long p = i->p;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + p % 10), list);
        p /= 10;
    } while (p);
    if (i->sign == NEG) {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '-'), list);
    }
    return list;
}

EhValue *ehratio_display(EhRt *rt, EhRatio *i) {
    EhValue *list = new_list_empty(rt, NULL);
    long q = i->q;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + q % 10), list);
        q /= 10;
    } while (q);
    list = new_list_cons(rt, NULL, new_char(rt, NULL, '/'), list);
    long p = i->p;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + p % 10), list);
        p /= 10;
    } while (p);
    if (i->sign == NEG) {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '-'), list);
    }
    return list;
}
