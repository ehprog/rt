#include <string.h>
#include "rt.h"
#include "value.h"
#include "hash_trie.h"
#include "hash.h"
#include "panic.h"

EhHnode *node_assoc(EhRt *rt, EhHnode *node, EhValue *key, EhValue *value, size_t *len, EhHashCode hash, unsigned int shift);
EhHnode *node_remove(EhRt *rt, EhHnode *node, EhValue *key, size_t *len, EhHashCode hash, unsigned int shift);
EhValue *node_find(EhRt *rt, EhHnode *node, EhValue *key, EhHashCode hash, unsigned int shift);

EhHtrie *new_htrie(EhRt *rt) {
    EhHtrie *trie = (EhHtrie *) ehgc_alloc(rt, &rt->gc, EH_HTRIE, sizeof(EhHtrie));
    trie->root = NULL;
    trie->len = 0;
    return trie;
}

EhHbranch *new_hbranch(EhRt *rt) {
    EhHbranch *branch;
    branch = (EhHbranch *) ehgc_alloc(rt, &rt->gc, EH_HNODE, sizeof(EhHbranch));
    branch->variant = EH_HTRIE_BRANCH;
    memset(branch->nodes, 0, sizeof(branch->nodes));
    return branch;
}

EhHleaf *new_hleaf(EhRt *rt, EhValue *key, EhValue *value) {
    EhHleaf *leaf;
    leaf = (EhHleaf *) ehgc_alloc(rt, &rt->gc, EH_HNODE, sizeof(EhHleaf));
    leaf->variant = EH_HTRIE_LEAF;
    leaf->key = key;
    leaf->value = value;
    return leaf;
}

EhHtrie *ehhtrie_assoc(EhRt *rt, EhHtrie *trie, EhValue *key, EhValue *value) {
    EhHasher hasher;
    new_hasher(&hasher);
    ehvalue_hash(rt, &hasher, key);
    EhHashCode hash = ehhasher_finalize(&hasher);
    size_t len = trie->len;
    EhHnode *newroot;
    if (trie->root) {
        newroot = node_assoc(rt, trie->root, key, value, &len, hash, 0);
    } else {
        newroot = (EhHnode*) new_hleaf(rt, key, value);
    }
    EhHtrie *new = new_htrie(rt);
    new->root = newroot;
    new->len = len;
    return new;
}

EhHtrie *ehhtrie_remove(EhRt *rt, EhHtrie *trie, EhValue *key) {
    EhHasher hasher;
    new_hasher(&hasher);
    ehvalue_hash(rt, &hasher, key);
    EhHashCode hash = ehhasher_finalize(&hasher);
    size_t len = trie->len;
    EhHnode *newroot = NULL;
    if (trie->root) {
        newroot = node_remove(rt, trie->root, key, &len, hash, 0);
    }
    EhHtrie *new = new_htrie(rt);
    new->root = newroot;
    new->len = len;
    return new;
}

EhValue *ehhtrie_find(EhRt *rt, EhHtrie *trie, EhValue *key) {
    EhHasher hasher;
    new_hasher(&hasher);
    ehvalue_hash(rt, &hasher, key);
    EhHashCode hash = ehhasher_finalize(&hasher);
    if (trie->root) {
        return node_find(rt, trie->root, key, hash, 0);
    } else {
        return NULL;
    }
}

int ehhtrie_contains(EhRt *rt, EhHtrie *trie, EhValue *key) {
    return ehhtrie_find(rt, trie, key) != NULL;
}

EhHnode *node_assoc(EhRt *rt, EhHnode *node, EhValue *key, EhValue *value, size_t *len, EhHashCode hash, unsigned int shift) {
    switch (node->variant) {
        case EH_HTRIE_LEAF:
            if (ehvalue_eq(rt, key, ((EhHleaf *) node)->key)) {
                return (EhHnode *) new_hleaf(rt, key, value);
            } else {
                EhHbranch *new;
                new = new_hbranch(rt);
                EhHasher hasher;
                new_hasher(&hasher);
                ehvalue_hash(rt, &hasher, ((EhHleaf *) node)->key);
                EhHashCode oldnode_hash = ehhasher_finalize(&hasher);
                size_t idx = (oldnode_hash >> shift) & EH_HTRIE_MASK;
                new->nodes[idx] = node;
                return node_assoc(rt, (EhHnode *) new, key, value, len, hash, shift);
            }
        case EH_HTRIE_BRANCH:
            {
                EhHbranch *new;
                new = new_hbranch(rt);
                memcpy(new->nodes, ((EhHbranch *) node)->nodes, sizeof(EhHnode *) * EH_HTRIE_NODES);
                size_t idx = (hash >> shift) & EH_HTRIE_MASK;
                EhHnode *next = ((EhHbranch *) node)->nodes[idx];
                if (next) {
                    new->nodes[idx] = node_assoc(rt, next, key, value, len, hash, shift + EH_HTRIE_SHIFT);
                } else {
                    *len += 1;
                    new->nodes[idx] = (EhHnode *) new_hleaf(rt, key, value);
                }
                return (EhHnode *) new;
            }
        default:
            unreachable(rt);
    }
}

EhHnode *node_remove(EhRt *rt, EhHnode *node, EhValue *key, size_t *len, EhHashCode hash, unsigned int shift) {
    switch (node->variant) {
        case EH_HTRIE_LEAF:
            if (ehvalue_eq(rt, key, ((EhHleaf *) node)->key)) {
                *len -= 1;
                return NULL;
            } else {
                return node;
            }
        case EH_HTRIE_BRANCH:
            {
                EhHbranch *new;
                new = new_hbranch(rt);
                memcpy(new->nodes, ((EhHbranch *) node)->nodes, sizeof(EhHnode *) * EH_HTRIE_NODES);
                size_t idx = (hash >> shift) & EH_HTRIE_MASK;
                EhHnode *next = ((EhHbranch *) node)->nodes[idx];
                if (next) {
                    new->nodes[idx] = node_remove(rt, next, key, len, hash, shift + EH_HTRIE_SHIFT);
                    return (EhHnode *) new;
                } else {
                    return node;
                }
            }
        default:
            unreachable(rt);
    }
}

EhValue *node_find(EhRt *rt, EhHnode *node, EhValue *key, EhHashCode hash, unsigned int shift) {
    switch (node->variant) {
        case EH_HTRIE_LEAF:
            if (ehvalue_eq(rt, key, ((EhHleaf *) node)->key)) {
                return ((EhHleaf *) node)->value;
            } else {
                return NULL;
            }
        case EH_HTRIE_BRANCH:
            {
                size_t idx = (hash >> shift) & EH_HTRIE_MASK;
                EhHnode *next = ((EhHbranch *) node)->nodes[idx];
                if (next) {
                    return node_find(rt, next, key, hash, shift + EH_HTRIE_SHIFT);
                } else {
                    return NULL;
                }
            }
        default:
            unreachable(rt);
    }
}
