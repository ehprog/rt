#include <string.h>
#include "value.h"
#include "types.h"
#include "rt.h"

EhType *new_type(EhRt *rt, EhType *type, int flags, EhTypeAtom *first, size_t restc, EhTypeExpr **restv) {
    size_t size = sizeof(EhType) + restc * sizeof(EhType *);
    if (!type) {
        type = malloc(size);
    }

    type->size = size;
    type->rc = 0;
    type->variant = EH_TYPE;
    type->type_id = -1;
    ((EhType *) type)->flags = flags;
    ((EhType *) type)->first = first;
    memcpy(((EhType *) type)->rest, restv, restc * sizeof(EhType *));

    ++first->rc;
    size_t len = (type->size - sizeof(EhType)) / sizeof(EhType *);
    for (size_t i = 0; i < len; i++) {
        ++restv[i]->rc;
    }

    return type;
}

void del_type(EhRt *rt, EhType *type) {
    type->rc -= 1;
    if (!type->rc) {
        size_t len;
        switch (type->variant) {
            case EH_TYPE:
                del_type(rt, (EhType *) type->first);
                len = (type->size - sizeof(EhType)) / sizeof(EhType *);
                for (size_t i = 0; i < len; i++) {
                    del_type(rt, (EhType *) type->rest[i]);
                }
                break;
            case EH_TYPE_ATOM:
                break;
        }
        free(type);
    }
}

EhTypeAtom *new_type_atom(EhRt *rt, EhTypeAtom *type, size_t len, char *name) {
    size_t size = sizeof(EhTypeAtom) + len;
    if (!type) {
        type = malloc(size);
    }

    type->size = size;
    type->rc = 0;
    type->variant = EH_TYPE_ATOM;
    type->type_id = -1;
    memcpy(((EhTypeAtom *) type)->name, name, len);

    return type;
}

int ehtype_matches(EhRt *rt, EhType *type, EhValue *value) {
    size_t unionc;
    switch (type->variant) {
        case EH_TYPE:
            if (type->flags & EHTYPE_UNIONALL) {
                return 1;
            } else if (type->flags & EHTYPE_UNION) {
                unionc = (type->size - sizeof(EhType)) / sizeof(EhTypeExpr *);
                for (size_t i = 0; i < unionc; i++) {
                    if (ehtype_matches(rt, (EhType *) type->rest[i], value)) {
                        return 1;
                    }
                }
                return 0;
            } else {
                return type->type_id == value->type;
            }
        case EH_TYPE_ATOM:
            return type->type_id == value->type;
    }
}
