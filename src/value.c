#include <string.h>
#include "value.h"
#include "hash.h"
#include "panic.h"
#include "types.h"
#include "ratio.h"
#include "gc.h"

static EhValue **destr(EhRt *rt, int *success, int *len, EhDestr *destr, EhValue **argv) {
    EhValue *arg = *argv;
    switch (arg->type) {
        case EH_NIL:
            *len = 0;
            *success = 1;
            return NULL;
        case EH_BOOL:
            *len = 0;
            *success = ((EhBool *) arg)->value == destr->variant;
            return NULL;
        case EH_INT:
            *len = 1;
            *success = 1;
            return argv;
        case EH_RATIO:
            *len = 1;
            *success = 1;
            return argv;
        case EH_REAL:
            *len = 1;
            *success = 1;
            return argv;
        case EH_CHAR:
            *len = 1;
            *success = 1;
            return argv;
        case EH_BIND:
            *len = 1;
            *success = 1;
            return argv;
        case EH_DESTR:
            *len = 1;
            *success = 1;
            return argv;
        case EH_LIST:
            if (((EhList *) arg)->variant == destr->variant) {
                *success = 1;
                switch (((EhList *) arg)->variant) {
                    case EH_LIST_EMPTY:
                        *len = 0;
                        return NULL;
                    case EH_LIST_CONS:
                        *len = 2;
                        return &((EhListCons *) arg)->head;
                }
            } else {
                *success = 0;
                return NULL;
            }
        case EH_HNODE:
            unimplemented(rt);
        case EH_HTRIE:
            unimplemented(rt);
        case EH_FUNCTION:
            unimplemented(rt);
        case EH_CLOSURE:
            unimplemented(rt);
        default:
            if (((EhData *) arg)->variant == destr->variant) {
                *len = (((EhData *) arg)->size - sizeof(EhData)) / sizeof(EhValue *);
                *success = 1;
                return ((EhData *) arg)->fieldv;
            } else {
                *success = 0;
                return NULL;
            }
    }
}

static int matches(EhRt *rt, EhValue **paramv, int *parami, int *outi, EhValue **out, int *argc, EhValue **argv, int *argi) {
    EhValue *param = paramv[*parami];
    EhValue *arg = argv[*argi];

    size_t len;
    EhType *param_type;
    switch (param->type) {
        case EH_NIL:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_NIL;
        case EH_BOOL:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_BOOL && ((EhBool *) param)->value == ((EhBool *) arg)->value;
        case EH_PTR:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_PTR && ((EhPtr *) param)->ptr == ((EhPtr *) arg)->ptr;
        case EH_INT:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_INT && ((EhInt *) param)->value == ((EhInt *) arg)->value;
        case EH_RATIO:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_RATIO && ehratio_eq(rt, (EhRatio *) param, (EhRatio *) arg);
        case EH_REAL:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_REAL && ((EhReal *) param)->value == ((EhReal *) arg)->value;
        case EH_CHAR:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return arg->type == EH_CHAR && ((EhChar *) param)->value == ((EhChar *) arg)->value;
        case EH_BIND:
            ++*parami;
            ++*argi;
            --*argc;
            param_type = rt->types[((EhBind *) param)->type_id];
            out[*outi] = arg;
            ++*outi;
            if (!ehtype_matches(rt, param_type, arg)) {
                return 0;
            } else {
                EhClosure *pred;
                if ((pred = ((EhBind *) param)->pred)) {
                    EhValue *p = ehclosure_call(rt, pred, 1, &arg);
                    switch (p->type) {
                        case EH_NIL:
                            return 0;
                        case EH_BOOL:
                            return ((EhBool *) p)->value;
                        default:
                            return 1;
                    }
                } else {
                    return 1;
                }
            }
        case EH_DESTR:
            ++*parami;
            --*argc;
            len = (param->size - sizeof(EhDestr)) / sizeof(EhValue *);
            param_type = rt->types[((EhBind *) param)->type_id];
            if (ehtype_matches(rt, param_type, arg)) {
                int destr_count = 0;
                int success = 0;
                EhValue **destr_args = destr(rt, &success, &destr_count, (EhDestr *) param, argv + *argi);
                if (!success || destr_count != len) {
                    return 0;
                }
                int destri = 0;
                int parami = 0;
                if (destr_count == 0) {
                    ++*argi;
                } else {
                    while (destr_count) {
                        if (!matches(rt, ((EhDestr *) param)->patterns, &parami, outi, out, &destr_count, destr_args, &destri)) {
                            return 0;
                        }
                    }
                }
                return 1;
            }
            return 0;
        case EH_LIST:
            ++*parami;
            ++*argi;
            --*argc;
            out[*outi] = arg;
            ++*outi;
            return ehlist_eq(rt, (EhList *) param, (EhList *) arg);
        case EH_HNODE:
            unimplemented(rt);
        case EH_HTRIE:
            unimplemented(rt);
        case EH_FUNCTION:
            unimplemented(rt);
        case EH_CLOSURE:
            unimplemented(rt);
        default:
            unimplemented(rt);
    }
}

static int pat_argc(EhRt *rt, EhValue *param) {
    int argc = 0;
    size_t len;
    switch (param->type) {
        case EH_DESTR:
            len = (param->size - sizeof(EhDestr)) / sizeof(EhValue *);
            for (size_t i = 0; i < len; i++) {
                argc += pat_argc(rt, ((EhDestr *) param)->patterns[i]);
            }
            return argc;
        default:
            return 1;
    }
}

static int function_argc(EhRt *rt, EhFunction *func) {
    assert(rt, (func->type == EH_FUNCTION) || (func->type == EH_IFUNC), "not a function");
    int len = func->paramc;
    int argc = 0;
    for (int i = 0; i < len; i++) {
        argc += pat_argc(rt, func->data[i]);
    }

    return argc;
}

static int function_match(EhRt *rt, EhFunction *func, int *outi, EhValue **out, int argc, EhValue **argv) {
    assert(rt, (func->type == EH_FUNCTION) || (func->type == EH_IFUNC), "not a function");
    int pati = 0;
    int argi = 0;
    while (argc) {
        int match;
        match = matches(rt, func->data, &pati, outi, out, &argc, argv, &argi);
        if (!match) {
            return 0;
        }
    }

    return 1;
}

int ehclosure_match(EhRt *rt, EhClosure *closure, EhFunction **dst, EhValue ***out, size_t *outc, int argc, EhValue **argv) {
    size_t implc = (closure->size - sizeof(EhClosure)) / sizeof(EhValue *);
    for (int i = 0; i < implc; i++) {
        EhFunction *impl = (EhFunction *) closure->implv[i];
        int fargc;
        fargc = function_argc(rt, impl);
        *out = malloc(sizeof(EhValue *) * fargc);
        int outi = 0;
        int match;
        match = function_match(rt, impl, &outi, *out, argc, argv);
        if (match) {
            if (dst) {
                *dst = impl;
            }
            *outc = outi;
            return 1;
        } else {
            free(*out);
        }
    }

    return 0;
}

static EhValue *function_call(EhRt *rt, EhFunction *func, EhValue **argv) {
    EhValue *result = func->func(rt, func->data + func->paramc, argv);
    return result;
}

static EhValue *mod_import(EhRt *rt, EhFunction *func) {
    func->func(rt, func->data + func->paramc, NULL);
    return ehrt_peek(rt, 0);
}

EhValue *ehclosure_call(EhRt *rt, EhClosure *closure, int argc, EhValue **argv) {
    assert(rt, closure->type == EH_CLOSURE, "not a function");
    size_t implc = (closure->size - sizeof(EhClosure)) / sizeof(EhValue *);
    jmp_buf jmp;
    int n;
    if ((n = setjmp(jmp))) {
        // tailcall
        --n;
        argv = rt->stack + rt->stack_len - n;
        argc = n;
    }
    for (int i = 0; i < implc; i++) {
        EhFunction *impl = (EhFunction *) closure->implv[i];
        int fargc;
        fargc = function_argc(rt, impl);
        EhValue **out = malloc(sizeof(EhValue *) * fargc);
        int outi = 0;
        int match;
        match = function_match(rt, impl, &outi, out, argc, argv);
        if (match) {
            EhValue *result;
            switch (impl->type) {
                case EH_FUNCTION:
                    ehrt_pushcall(rt, closure, impl->func, out, argc, &jmp);
                    result = function_call(rt, impl, out);
                    ehrt_popcall(rt, NULL);
                    free(out);
                    return result;
                default:
                    panic(rt, "invalid function type found");
            }
        } else {
            free(out);
        }
    }

    panic(rt, "mismatch in function call");
}

EhValue *ehclosure_tailcall(EhRt *rt, EhClosure *closure, int argc, EhValue **argv) {
    struct EhCallFrame frame;
    while (ehrt_popcall(rt, &frame) != closure) {
        free(frame.out);
    }
    free(frame.out);
    for (int i = 0; i < argc; i++) {
        ehrt_push(rt, argv[i]);
    }
    longjmp(frame.jmp, argc + 1);
}

EhValue *ehmod_import(EhRt *rt, EhClosure *closure) {
    assert(rt, closure->type == EH_CLOSURE, "not a function");
    EhFunction *impl = (EhFunction *) closure->implv[0];
    switch (impl->type) {
        case EH_FUNCTION:
            return mod_import(rt, impl);
        case EH_IFUNC:
            return rt->interp_import(rt, rt->interp, (EhIFunc *) impl);
        default:
            panic(rt, "invalid function type found");
    }
}

EhClosure *ehclosure_impl(EhRt *rt, EhClosure *closure, EhValue *impl) {
    assert(rt, closure->type == EH_CLOSURE, "not a function");
    assert(rt, impl->type == EH_FUNCTION || impl->type == EH_IFUNC, "not an impl");

    size_t old_implc = (closure->size - sizeof(EhClosure)) / sizeof(EhValue *);
    size_t new_implc = old_implc + 1;
    EhClosure *new;
    new = (EhClosure *) ehgc_alloc(rt, &rt->gc, EH_CLOSURE, sizeof(EhClosure) + new_implc * sizeof(EhValue *));

    new->flags = closure->flags;
    memcpy(new->implv, closure->implv, old_implc * sizeof(EhValue *));
    new->implv[old_implc] = impl;

    return new;
}

EhValue *ehvar_get(EhRt *rt, EhVar *var) {
    assert(rt, var->type == EH_VAR, "not a var");
    return var->inner;
}

void ehvar_set(EhRt *rt, EhVar *var, EhValue *inner) {
    assert(rt, var->type == EH_VAR, "not a var");
    var->inner = inner;
}

int ehvalue_eq(EhRt *rt, const EhValue *a, const EhValue *b) {
    if (a->type != b->type) {
        return 0;
    }

    switch (a->type) {
        case EH_NIL:
            return ehnil_eq(rt, (EhNil *) a, (EhNil *) b);
        case EH_ATOM:
            return ehatom_eq(rt, (EhAtom *) a, (EhAtom *) b);
        case EH_BOOL:
            return ehbool_eq(rt, (EhBool *) a, (EhBool *) b);
        case EH_PTR:
            return ehptr_eq(rt, (EhPtr *) a, (EhPtr *) b);
        case EH_INT:
            return ehint_eq(rt, (EhInt *) a, (EhInt *) b);
        case EH_RATIO:
            return ehratio_eq(rt, (EhRatio *) a, (EhRatio *) b);
        case EH_REAL:
            return ehreal_eq(rt, (EhReal *) a, (EhReal *) b);
        case EH_CHAR:
            return ehchar_eq(rt, (EhChar *) a, (EhChar *) b);
        case EH_LIST:
            return ehlist_eq(rt, (EhList *) a, (EhList *) b);
        default:
            unimplemented(rt);
    }
}

int ehnil_eq(EhRt *rt, const EhNil *a, const EhNil *b) {
    return 1;
}

int ehatom_eq(EhRt *rt, const EhAtom *a, const EhAtom *b) {
    if (a->size != b->size) {
        return 0;
    }
    for (int i = 0; i < a->size - sizeof(EhAtom); i++) {
        if (a->atom[i] != b->atom[i]) {
            return 0;
        }
    }
    return 1;
}

int ehbool_eq(EhRt *rt, const EhBool *a, const EhBool *b) {
    return a->value == b->value;
}

int ehptr_eq(EhRt *rt, const EhPtr *a, const EhPtr *b) {
    return a->ptr == b->ptr;
}

int ehint_eq(EhRt *rt, const EhInt *a, const EhInt *b) {
    return a->value == b->value;
}

int ehreal_eq(EhRt *rt, const EhReal *a, const EhReal *b) {
    return a->value == b->value;
}

int ehchar_eq(EhRt *rt, const EhChar *a, const EhChar *b) {
    return a->value == b->value;
}

int ehlist_eq(EhRt *rt, const EhList *a, const EhList *b) {
    if (a->variant == EH_LIST_EMPTY && 
        b->variant == EH_LIST_EMPTY) {
        return 1;
    } else if (a->variant == EH_LIST_CONS && 
        b->variant == EH_LIST_CONS) {
        return ehvalue_eq(rt, ((EhListCons *) a)->head, ((EhListCons *) b)->head) &&
                ehlist_eq(rt, ((EhListCons *) a)->tail, ((EhListCons *) b)->tail);
    } else {
        return 0;
    }
}

int ehvalue_ne(EhRt *rt, const EhValue *a, const EhValue *b) {
    return !ehvalue_eq(rt, a, b);
}

int ehnil_ne(EhRt *rt, const EhNil *a, const EhNil *b) {
    return !ehnil_eq(rt, a, b);
}

int ehatom_ne(EhRt *rt, const EhAtom *a, const EhAtom *b) {
    return !ehatom_eq(rt, a, b);
}

int ehbool_ne(EhRt *rt, const EhBool *a, const EhBool *b) {
    return !ehbool_eq(rt, a, b);
}

int ehptr_ne(EhRt *rt, const EhPtr *a, const EhPtr *b) {
    return !ehptr_eq(rt, a, b);
}

int ehint_ne(EhRt *rt, const EhInt *a, const EhInt *b) {
    return !ehint_eq(rt, a, b);
}

int ehreal_ne(EhRt *rt, const EhReal *a, const EhReal *b) {
    return !ehreal_eq(rt, a, b);
}

int ehchar_ne(EhRt *rt, const EhChar *a, const EhChar *b) {
    return !ehchar_eq(rt, a, b);
}

int ehlist_ne(EhRt *rt, const EhList *a, const EhList *b) {
    return !ehlist_eq(rt, a, b);
}

int ehint_lt(EhRt *rt, const EhInt *a, const EhInt *b) {
    return a->value < b->value;
}

int ehreal_lt(EhRt *rt, const EhReal *a, const EhReal *b) {
    return a->value < b->value;
}

int ehint_gt(EhRt *rt, const EhInt *a, const EhInt *b) {
    return a->value > b->value;
}

int ehreal_gt(EhRt *rt, const EhReal *a, const EhReal *b) {
    return a->value > b->value;
}

int ehint_le(EhRt *rt, const EhInt *a, const EhInt *b) {
    return a->value <= b->value;
}

int ehreal_le(EhRt *rt, const EhReal *a, const EhReal *b) {
    return a->value <= b->value;
}

int ehint_ge(EhRt *rt, const EhInt *a, const EhInt *b) {
    return a->value >= b->value;
}

int ehreal_ge(EhRt *rt, const EhReal *a, const EhReal *b) {
    return a->value >= b->value;
}

void ehvalue_hash(EhRt *rt, EhHasher *state, const EhValue *a) {
    switch (a->type) {
        case EH_NIL:
            ehnil_hash(rt, state, (EhNil *) a);
            break;
        case EH_ATOM:
            ehatom_hash(rt, state, (EhAtom *) a);
            break;
        case EH_BOOL:
            ehbool_hash(rt, state, (EhBool *) a);
            break;
        case EH_PTR:
            ehptr_hash(rt, state, (EhPtr *) a);
            break;
        case EH_INT:
            ehint_hash(rt, state, (EhInt *) a);
            break;
        case EH_RATIO:
            ehratio_hash(rt, state, (EhRatio *) a);
            break;
        case EH_REAL:
            ehreal_hash(rt, state, (EhReal *) a);
            break;
        case EH_CHAR:
            ehchar_hash(rt, state, (EhChar *) a);
            break;
        default:
            unimplemented(rt);
    }
}

void ehnil_hash(EhRt *rt, EhHasher *state, const EhNil *a) {
    ehhasher_update_u64(state, a->type);
}

void ehatom_hash(EhRt *rt, EhHasher *state, const EhAtom *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_byte_array(state, a->atom, a->size - sizeof(EhAtom));
}

void ehbool_hash(EhRt *rt, EhHasher *state, const EhBool *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_u64(state, a->value);
}

void ehptr_hash(EhRt *rt, EhHasher *state, const EhPtr *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_u64(state, (uint64_t) a->ptr);
}

void ehint_hash(EhRt *rt, EhHasher *state, const EhInt *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_u64(state, a->value);
}

void ehreal_hash(EhRt *rt, EhHasher *state, const EhReal *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_f64(state, a->value);
}

void ehchar_hash(EhRt *rt, EhHasher *state, const EhChar *a) {
    ehhasher_update_u64(state, a->type);
    ehhasher_update_u64(state, a->value);
}

EhValue *new_nil(EhRt *rt, EhNil *nil) {
    if (!nil) {
        nil = (EhNil *) ehgc_alloc(rt, &rt->gc, EH_NIL, sizeof(EhNil));
    }

    return (EhValue *) nil;
}

EhValue *new_atom(EhRt *rt, EhAtom *atom, size_t len, const char *pstr) {
    if (!atom) {
        atom = (EhAtom *) ehgc_alloc(rt, &rt->gc, EH_ATOM, sizeof(EhAtom) + len);
    }

    memcpy(atom->atom, pstr, len);

    return (EhValue *) atom;
}

EhValue *new_bool(EhRt *rt, EhBool *p, const enum EhBoolTag variant) {
    if (!p) {
        p = (EhBool *) ehgc_alloc(rt, &rt->gc, EH_BOOL, sizeof(EhBool));
    }

    p->value = variant;

    return (EhValue *) p;
}

EhValue *new_ptr(EhRt *rt, EhPtr *i, void *ptr) {
    if (!i) {
        i = (EhPtr *) ehgc_alloc(rt, &rt->gc, EH_PTR, sizeof(EhPtr));
    }

    i->ptr = ptr;

    return (EhValue *) i;
}

EhValue *new_int(EhRt *rt, EhInt *i, const long value) {
    if (!i) {
        i = (EhInt *) ehgc_alloc(rt, &rt->gc, EH_INT, sizeof(EhInt));
    }

    i->value = value;

    return (EhValue *) i;
}

EhValue *new_real(EhRt *rt, EhReal *f, const double value) {
    if (!f) {
        f = (EhReal *) ehgc_alloc(rt, &rt->gc, EH_REAL, sizeof(EhReal));
    }

    f->value = value;

    return (EhValue *) f;
}

EhValue *new_char(EhRt *rt, EhChar *ch, const unsigned int value) {
    if (!ch) {
        ch = (EhChar *) ehgc_alloc(rt, &rt->gc, EH_CHAR, sizeof(EhChar));
    }

    ch->value = value;

    return (EhValue *) ch;
}

EhValue *new_list_empty(EhRt *rt, EhList *empty) {
    if (!empty) {
        empty = (EhList *) ehgc_alloc(rt, &rt->gc, EH_LIST, sizeof(EhListEmpty));
    }

    empty->variant = EH_LIST_EMPTY;

    return (EhValue *) empty;
}

EhValue *new_list_cons(EhRt *rt, EhList *cons, EhValue *head, EhValue *tail) {
    if (!cons) {
        cons = (EhList *) ehgc_alloc(rt, &rt->gc, EH_LIST, sizeof(EhListCons));
    }

    cons->variant = EH_LIST_CONS;
    ((EhListCons *) cons)->head = head;
    ((EhListCons *) cons)->tail = (EhList *) tail;

    return (EhValue *) cons;
}

EhValue *new_bind(EhRt *rt, EhBind *bind, unsigned int type_id, EhClosure *pred) {
    if (!bind) {
        bind = (EhBind *) ehgc_alloc(rt, &rt->gc, EH_BIND, sizeof(EhBind));
    }

    bind->type_id = type_id;
    bind->pred = pred;

    return (EhValue *) bind;
}

EhValue *new_destr(EhRt *rt, EhDestr *destr, unsigned int type_id, unsigned int variant, size_t patternc, EhValue **patternv) {
    if (!destr) {
        destr = (EhDestr *) ehgc_alloc(rt, &rt->gc, EH_DESTR, sizeof(EhDestr) + patternc * sizeof(EhValue *));
    }

    destr->type_id = type_id;
    destr->variant = variant;
    memcpy(destr->patterns, patternv, patternc * sizeof(EhValue *));

    return (EhValue *) destr;
}

EhValue *new_closure(EhRt *rt, EhClosure *closure, int flags, unsigned int implc, EhFunction **implv) {
    if (!closure) {
        closure = (EhClosure *) ehgc_alloc(rt, &rt->gc, EH_CLOSURE, sizeof(EhClosure) + implc * sizeof(EhValue *));
    }

    closure->flags = flags;
    memcpy(closure->implv, implv, implc * sizeof(EhValue *));

    return (EhValue *) closure;
}

EhValue *new_function(EhRt *rt, EhFunction *function, EhFunc func, EhHtrie *env, size_t paramc, EhValue **paramv, size_t datac, EhValue **data) {
    if (!function) {
        function = (EhFunction *) ehgc_alloc(rt, &rt->gc, EH_FUNCTION, sizeof(EhFunction) + paramc * sizeof(EhValue *) + datac * sizeof(EhValue *));
    }

    function->func = func;
    function->env = env;
    function->paramc = paramc;
    function->datac = datac;
    memcpy(function->data, paramv, paramc * sizeof(EhValue *));
    memcpy(function->data + paramc, data, datac * sizeof(EhValue *));

    return (EhValue *) function;
}

EhValue *new_ifunc(EhRt *rt, EhIFunc *function, size_t func, EhHtrie *env, size_t paramc, EhValue **paramv, size_t datac, EhValue **data) {
    if (!function) {
        function = (EhIFunc *) ehgc_alloc(rt, &rt->gc, EH_IFUNC, sizeof(EhIFunc) + paramc * sizeof(EhValue *) + datac * sizeof(EhValue *));
    }

    function->func = func;
    function->env = env;
    function->paramc = paramc;
    function->datac = datac;
    memcpy(function->data, paramv, paramc * sizeof(EhValue *));
    memcpy(function->data + paramc, data, datac * sizeof(EhValue *));

    return (EhValue *) function;
}

EhValue *new_var(EhRt *rt, EhVar *var, EhValue *inner) {
    if (!var) {
        var = (EhVar *) ehgc_alloc(rt, &rt->gc, EH_VAR, sizeof(EhVar));
    }

    var->inner = inner;

    return (EhValue *) var;
}

EhValue *new_data(EhRt *rt, EhData *data, unsigned int type, unsigned int variant, size_t fieldc, EhValue **fieldv) {
    if (!data) {
        data = (EhData *) ehgc_alloc(rt, &rt->gc, type, sizeof(EhData) + fieldc * sizeof(EhValue *));
    }

    data->variant = variant;
    memcpy(data->fieldv, fieldv, fieldc * sizeof(EhValue *));

    return (EhValue *) data;
}

EhValue *ehatom_from_cstr(EhRt *rt, EhAtom *atom, const char *cstr) {
    size_t len = strlen(cstr);
    return new_atom(rt, atom, len, cstr);
}

static EhValue *list_from_values(EhRt *rt, size_t c, EhValue **v) {
    EhValue *list = new_list_empty(rt, NULL);
    for (size_t i = c - 1; i < c; i--) {
        list = new_list_cons(rt, NULL, v[i], list);
    }
    return list;
}

EhValue *ehstr_from_pstr(EhRt *rt, EhList *list, size_t len, const char *pstr) {
    if (!list) {
        list = (EhList *) new_list_empty(rt, NULL);
    }
    ehgc_disable(rt, &rt->gc);
    for (size_t i = len - 1; i < len; i--) {
        list = (EhList *) new_list_cons(rt, NULL, new_char(rt, NULL, pstr[i]), (EhValue *) list);
    }
    ehgc_enable(rt, &rt->gc);
    return (EhValue *) list;
}

EhValue *ehlist_concat(EhRt *rt, EhList *a, EhList *b) {
    EhList *listv[] = { a, b };
    return ehlist_concatv(rt, 2, listv);
}

EhValue *ehlist_concatv(EhRt *rt, size_t listc, EhList **listv) {
    EhValue *list = new_list_empty(rt, NULL);
    EhValue **last = &list;
    ehgc_disable(rt, &rt->gc);
    for (size_t i = 0; i < listc; i++) {
        EhList *a = listv[i];
        while (a->variant == EH_LIST_CONS) {
            *last = new_list_cons(rt, NULL, ((EhListCons *) a)->head, *last);
            last = (EhValue **) &((EhListCons *) (*last))->tail;
            a = ((EhListCons *) a)->tail;
        }
    }
    ehgc_enable(rt, &rt->gc);
    return list;
}

EhValue *ehvalue_repr(EhRt *rt, EhValue *value) {
    switch (value->type) {
        case EH_NIL:
            return ehnil_repr(rt, (EhNil *) value);
        case EH_ATOM:
            return ehatom_repr(rt, (EhAtom *) value);
        case EH_BOOL:
            return ehbool_repr(rt, (EhBool *) value);
        case EH_PTR:
            return ehptr_repr(rt, (EhPtr *) value);
        case EH_INT:
            return ehint_repr(rt, (EhInt *) value);
        case EH_RATIO:
            return ehratio_repr(rt, (EhRatio *) value);
        case EH_REAL:
            return ehreal_repr(rt, (EhReal *) value);
        case EH_CHAR:
            return ehchar_repr(rt, (EhChar *) value);
        default:
            unimplemented(rt);
    }
}

EhValue *ehnil_repr(EhRt *rt, EhNil *nil) {
    ehgc_disable(rt, &rt->gc);
    EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'N'),
            new_list_cons(rt, NULL, new_char(rt, NULL, 'i'),
            new_list_cons(rt, NULL, new_char(rt, NULL, 'l'),
            new_list_empty(rt, NULL))));
    ehgc_enable(rt, &rt->gc);
    return string;
}

EhValue *ehatom_repr(EhRt *rt, EhAtom *atom) {
    size_t len = atom->size - sizeof(EhAtom);
    return ehstr_from_pstr(rt, NULL, len, atom->atom);
}

EhValue *ehbool_repr(EhRt *rt, EhBool *p) {
    if (p->value) {
        ehgc_disable(rt, &rt->gc);
        EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'T'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'r'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'u'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'e'),
                new_list_empty(rt, NULL)))));
        ehgc_enable(rt, &rt->gc);
        return string;
    } else {
        ehgc_disable(rt, &rt->gc);
        EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'F'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'a'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'l'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 's'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'e'),
                new_list_empty(rt, NULL))))));
        ehgc_enable(rt, &rt->gc);
        return string;
    }
}

EhValue *ehptr_repr(EhRt *rt, EhPtr *i) {
    ehgc_disable(rt, &rt->gc);
    EhValue *list = new_list_empty(rt, NULL);
    unsigned long value = (unsigned long) i->ptr;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + value & 0xf), list);
        value >>= 4;
    } while (value);
    ehgc_enable(rt, &rt->gc);
    return list;
}

EhValue *ehint_repr(EhRt *rt, EhInt *i) {
    ehgc_disable(rt, &rt->gc);
    EhValue *list = new_list_empty(rt, NULL);
    long value = i->value;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + value % 10), list);
        value /= 10;
    } while (value);
    ehgc_enable(rt, &rt->gc);
    return list;
}

EhValue *ehreal_repr(EhRt *rt, EhReal *x) {
    unimplemented(rt)
}

EhValue *ehchar_repr(EhRt *rt, EhChar *c) {
    ehgc_disable(rt, &rt->gc);
    EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, '\''),
            new_list_cons(rt, NULL, (EhValue *) c,
            new_list_cons(rt, NULL, new_char(rt, NULL, '\''),
            new_list_empty(rt, NULL))));
    ehgc_enable(rt, &rt->gc);
    return string;
}

EhValue *ehlist_repr(EhRt *rt, EhList *l) {
    unimplemented(rt)
}

EhValue *ehbind_repr(EhRt *rt, EhBind *b) {
    unimplemented(rt)
}

EhValue *ehdestr_repr(EhRt *rt, EhDestr *d) {
    unimplemented(rt)
}

EhValue *ehclosure_repr(EhRt *rt, EhClosure *f) {
    unimplemented(rt)
}

EhValue *ehvar_repr(EhRt *rt, EhVar *f) {
    ehgc_disable(rt, &rt->gc);
    EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'V'),
            new_list_cons(rt, NULL, new_char(rt, NULL, 'a'),
            new_list_cons(rt, NULL, new_char(rt, NULL, 'r'),
            new_list_cons(rt, NULL, new_char(rt, NULL, ' '),
            ehvalue_repr(rt, f->inner)))));
    ehgc_enable(rt, &rt->gc);
    return string;
}

EhValue *ehvalue_display(EhRt *rt, EhValue *value) {
    switch (value->type) {
        case EH_NIL:
            return ehnil_display(rt, (EhNil *) value);
        case EH_ATOM:
            return ehatom_display(rt, (EhAtom *) value);
        case EH_BOOL:
            return ehbool_display(rt, (EhBool *) value);
        case EH_PTR:
            return ehptr_display(rt, (EhPtr *) value);
        case EH_INT:
            return ehint_display(rt, (EhInt *) value);
        case EH_RATIO:
            return ehratio_display(rt, (EhRatio *) value);
        case EH_REAL:
            return ehreal_display(rt, (EhReal *) value);
        case EH_CHAR:
            return ehchar_display(rt, (EhChar *) value);
        default:
            unimplemented(rt);
    }
}

EhValue *ehnil_display(EhRt *rt, EhNil *nil) {
    ehgc_disable(rt, &rt->gc);
    EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'N'),
            new_list_cons(rt, NULL, new_char(rt, NULL, 'i'),
            new_list_cons(rt, NULL, new_char(rt, NULL, 'l'),
            new_list_empty(rt, NULL))));
    ehgc_enable(rt, &rt->gc);
    return string;
}

EhValue *ehatom_display(EhRt *rt, EhAtom *atom) {
    size_t len = atom->size - sizeof(EhAtom);
    return ehstr_from_pstr(rt, NULL, len, atom->atom);
}

EhValue *ehbool_display(EhRt *rt, EhBool *p) {
    if (p->value) {
        ehgc_disable(rt, &rt->gc);
        EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'T'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'r'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'u'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'e'),
                new_list_empty(rt, NULL)))));
        ehgc_enable(rt, &rt->gc);
        return string;
    } else {
        ehgc_disable(rt, &rt->gc);
        EhValue *string = new_list_cons(rt, NULL, new_char(rt, NULL, 'F'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'a'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'l'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 's'),
                new_list_cons(rt, NULL, new_char(rt, NULL, 'e'),
                new_list_empty(rt, NULL))))));
        ehgc_enable(rt, &rt->gc);
        return string;
    }
}

EhValue *ehptr_display(EhRt *rt, EhPtr *i) {
    ehgc_disable(rt, &rt->gc);
    EhValue *list = new_list_empty(rt, NULL);
    unsigned long value = (unsigned long) i->ptr;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + value & 0xf), list);
        value >>= 4;
    } while (value);
    ehgc_enable(rt, &rt->gc);
    return list;
}

EhValue *ehint_display(EhRt *rt, EhInt *i) {
    ehgc_disable(rt, &rt->gc);
    EhValue *list = new_list_empty(rt, NULL);
    long value = i->value;
    do {
        list = new_list_cons(rt, NULL, new_char(rt, NULL, '0' + value % 10), list);
        value /= 10;
    } while (value);
    ehgc_enable(rt, &rt->gc);
    return list;
}

EhValue *ehreal_display(EhRt *rt, EhReal *x) {
    unimplemented(rt)
}

EhValue *ehchar_display(EhRt *rt, EhChar *c) {
    ehgc_disable(rt, &rt->gc);
    EhValue *string = new_list_cons(rt, NULL, (EhValue *) c, new_list_empty(rt, NULL));
    ehgc_enable(rt, &rt->gc);
    return string;
}

EhValue *ehlist_display(EhRt *rt, EhList *l) {
    unimplemented(rt)
}

// TODO(walterpi): write utf-8
void ehstr_write(EhRt *rt, EhList *str) {
    while (str->variant == EH_LIST_CONS) {
        assert_eq(rt, ((EhListCons *) str)->head->type, EH_CHAR, "list is not a string: %d", ((EhListCons *) str)->head->type);
        fwrite(&((EhChar *) ((EhListCons *) str)->head)->value, 1, 1, stdout);
        str = ((EhListCons *) str)->tail;
    }
}

// TODO(walterpi): read utf-8
EhValue *ehchar_read(EhRt *rt) {
    char c;
    fread(&c, 1, 1, stdin);
    return new_char(rt, NULL, c);
}
