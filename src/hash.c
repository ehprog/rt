#include "hash.h"

EhHasher *new_hasher(EhHasher *state) {
    if (!state) {
        state = malloc(sizeof(EhHasher));
    }

    state->hash = 0;

    return state;
}

EhHashCode ehhasher_finalize(EhHasher *state) {
    return state->hash;
}

// jenkins hash
void ehhasher_update_byte_array(EhHasher *state, void const *ptr, size_t len) {
    char const *bytes = ptr;
    size_t i = 0;
    EhHashCode hash = state->hash;
    while (i < len) {
        hash += bytes[i++];
        hash += hash << 10;
        hash ^= hash >> 6;
    }
    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;
    state->hash = hash;
}

void ehhasher_update_cstr(EhHasher *state, char const *str) {
    EhHashCode hash = state->hash;
    while (*str) {
        hash += *str;
        hash += hash << 10;
        hash ^= hash >> 6;
        ++str;
    }
    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;
    state->hash = hash;
}

void ehhasher_update_u64(EhHasher *state, uint64_t u64) {
    ehhasher_update_byte_array(state, &u64, sizeof(uint64_t));
}

void ehhasher_update_f64(EhHasher *state, double f64) {
    ehhasher_update_byte_array(state, &f64, sizeof(double));
}
