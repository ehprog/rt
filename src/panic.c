#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <execinfo.h>
#include "panic.h"
#include "rt.h"

_Noreturn
void _panic(EhRt *rt) {
    rt->except.panicking = 1;
    int backtrace;
    char *eh_backtrace;
    if ((eh_backtrace = getenv("EH_BACKTRACE"))) {
        if (strcmp(eh_backtrace, "0") == 0) {
            backtrace = 0;
        } else {
            backtrace = 1;
        }
    } else {
        backtrace = 0;
    }
    if (backtrace) {
        fprintf(stderr, "stack backtrace:\n");
        for (int i = rt->csp - 1; i >= 0; i--) {
            struct EhCallFrame *frame = rt->call_stack + i;
            EhDebugFrame debug;
            if (ehdebug_find(&rt->debug, frame->func, &debug)) {
                printf(
                    "%4lu: [0x%p] %s (%s:%d:%d)\n",
                    rt->csp - i,
                    frame->func,
                    debug.ident,
                    debug.file,
                    debug.line,
                    debug.column
                );
            } else {
                fprintf(stderr, "could not print rest of the call stack\n");
                exit(1);
            }
        }
    } else {
        fprintf(stderr, "note: Run with `EH_BACKTRACE=1` environment variable to display a backtrace.\n");
    }
    longjmp(rt->except.jmp, 1);
}
