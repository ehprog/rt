#include <math.h>
#include "value.h"
#include "ratio.h"
#include "rt.h"
#include "intrinsic.h"
#include "panic.h"

EhValue *ehintr_nil_eq(EhRt *rt, EhValue **data, EhValue **argv) {
    int eq = ehnil_eq(rt, (EhNil *) argv[0], (EhNil *) argv[1]);
    return new_bool(rt, NULL, eq);
}

EhValue *ehintr_bool_eq(EhRt *rt, EhValue **data, EhValue **argv) {
    int eq = ehbool_eq(rt, (EhBool *) argv[0], (EhBool *) argv[1]);
    return new_bool(rt, NULL, eq);
}

EhValue *ehintr_int_eq(EhRt *rt, EhValue **data, EhValue **argv) {
    int eq = ehint_eq(rt, (EhInt *) argv[0], (EhInt *) argv[1]);
    return new_bool(rt, NULL, eq);
}

EhValue *ehintr_ratio_eq(EhRt *rt, EhValue **data, EhValue **argv) {
    int eq = ehratio_eq(rt, (EhRatio *) argv[0], (EhRatio *) argv[1]);
    return new_bool(rt, NULL, eq);
}

EhValue *ehintr_real_eq(EhRt *rt, EhValue **data, EhValue **argv) {
    int eq = ehreal_eq(rt, (EhReal *) argv[0], (EhReal *) argv[1]);
    return new_bool(rt, NULL, eq);
}

EhValue *ehintr_char_eq(EhRt *rt, EhValue **data, EhValue **argv) {
    int eq = ehchar_eq(rt, (EhChar *) argv[0], (EhChar *) argv[1]);
    return new_bool(rt, NULL, eq);
}

EhValue *ehintr_nil_ne(EhRt *rt, EhValue **data, EhValue **argv) {
    int ne = ehnil_ne(rt, (EhNil *) argv[0], (EhNil *) argv[1]);
    return new_bool(rt, NULL, ne);
}

EhValue *ehintr_bool_ne(EhRt *rt, EhValue **data, EhValue **argv) {
    int ne = ehbool_ne(rt, (EhBool *) argv[0], (EhBool *) argv[1]);
    return new_bool(rt, NULL, ne);
}

EhValue *ehintr_int_ne(EhRt *rt, EhValue **data, EhValue **argv) {
    int ne = ehint_ne(rt, (EhInt *) argv[0], (EhInt *) argv[1]);
    return new_bool(rt, NULL, ne);
}

EhValue *ehintr_ratio_ne(EhRt *rt, EhValue **data, EhValue **argv) {
    int ne = ehratio_ne(rt, (EhRatio *) argv[0], (EhRatio *) argv[1]);
    return new_bool(rt, NULL, ne);
}

EhValue *ehintr_real_ne(EhRt *rt, EhValue **data, EhValue **argv) {
    int ne = ehreal_ne(rt, (EhReal *) argv[0], (EhReal *) argv[1]);
    return new_bool(rt, NULL, ne);
}

EhValue *ehintr_char_ne(EhRt *rt, EhValue **data, EhValue **argv) {
    int ne = ehchar_ne(rt, (EhChar *) argv[0], (EhChar *) argv[1]);
    return new_bool(rt, NULL, ne);
}

EhValue *ehintr_int_le(EhRt *rt, EhValue **data, EhValue **argv) {
    int le = ehint_le(rt, (EhInt *) argv[0], (EhInt *) argv[1]);
    return new_bool(rt, NULL, le);
}

EhValue *ehintr_ratio_le(EhRt *rt, EhValue **data, EhValue **argv) {
    int le = ehratio_le(rt, (EhRatio *) argv[0], (EhRatio *) argv[1]);
    return new_bool(rt, NULL, le);
}

EhValue *ehintr_real_le(EhRt *rt, EhValue **data, EhValue **argv) {
    int le = ehreal_le(rt, (EhReal *) argv[0], (EhReal *) argv[1]);
    return new_bool(rt, NULL, le);
}

EhValue *ehintr_int_ge(EhRt *rt, EhValue **data, EhValue **argv) {
    int ge = ehint_ge(rt, (EhInt *) argv[0], (EhInt *) argv[1]);
    return new_bool(rt, NULL, ge);
}

EhValue *ehintr_ratio_ge(EhRt *rt, EhValue **data, EhValue **argv) {
    int ge = ehratio_ge(rt, (EhRatio *) argv[0], (EhRatio *) argv[1]);
    return new_bool(rt, NULL, ge);
}

EhValue *ehintr_real_ge(EhRt *rt, EhValue **data, EhValue **argv) {
    int ge = ehreal_ge(rt, (EhReal *) argv[0], (EhReal *) argv[1]);
    return new_bool(rt, NULL, ge);
}

EhValue *ehintr_int_lt(EhRt *rt, EhValue **data, EhValue **argv) {
    int lt = ehint_lt(rt, (EhInt *) argv[0], (EhInt *) argv[1]);
    return new_bool(rt, NULL, lt);
}

EhValue *ehintr_ratio_lt(EhRt *rt, EhValue **data, EhValue **argv) {
    int lt = ehratio_lt(rt, (EhRatio *) argv[0], (EhRatio *) argv[1]);
    return new_bool(rt, NULL, lt);
}

EhValue *ehintr_real_lt(EhRt *rt, EhValue **data, EhValue **argv) {
    int lt = ehreal_lt(rt, (EhReal *) argv[0], (EhReal *) argv[1]);
    return new_bool(rt, NULL, lt);
}

EhValue *ehintr_int_gt(EhRt *rt, EhValue **data, EhValue **argv) {
    int gt = ehint_gt(rt, (EhInt *) argv[0], (EhInt *) argv[1]);
    return new_bool(rt, NULL, gt);
}

EhValue *ehintr_ratio_gt(EhRt *rt, EhValue **data, EhValue **argv) {
    int gt = ehratio_gt(rt, (EhRatio *) argv[0], (EhRatio *) argv[1]);
    return new_bool(rt, NULL, gt);
}

EhValue *ehintr_real_gt(EhRt *rt, EhValue **data, EhValue **argv) {
    int gt = ehreal_gt(rt, (EhReal *) argv[0], (EhReal *) argv[1]);
    return new_bool(rt, NULL, gt);
}

EhValue *ehintr_nil_hash(EhRt *rt, EhValue **data, EhValue **argv) {
    EhHasher state;
    state.hash = ((EhInt *) argv[0])->value;
    ehnil_hash(rt, &state, (EhNil *) argv[1]);
    return new_int(rt, NULL, state.hash);
}

EhValue *ehintr_bool_hash(EhRt *rt, EhValue **data, EhValue **argv) {
    EhHasher state;
    state.hash = ((EhInt *) argv[0])->value;
    ehbool_hash(rt, &state, (EhBool *) argv[1]);
    return new_int(rt, NULL, state.hash);
}

EhValue *ehintr_int_hash(EhRt *rt, EhValue **data, EhValue **argv) {
    EhHasher state;
    state.hash = ((EhInt *) argv[0])->value;
    ehint_hash(rt, &state, (EhInt *) argv[1]);
    return new_int(rt, NULL, state.hash);
}

EhValue *ehintr_ratio_hash(EhRt *rt, EhValue **data, EhValue **argv) {
    EhHasher state;
    state.hash = ((EhInt *) argv[0])->value;
    ehratio_hash(rt, &state, (EhRatio *) argv[1]);
    return new_int(rt, NULL, state.hash);
}

EhValue *ehintr_real_hash(EhRt *rt, EhValue **data, EhValue **argv) {
    EhHasher state;
    state.hash = ((EhInt *) argv[0])->value;
    ehreal_hash(rt, &state, (EhReal *) argv[1]);
    return new_int(rt, NULL, state.hash);
}

EhValue *ehintr_char_hash(EhRt *rt, EhValue **data, EhValue **argv) {
    EhHasher state;
    state.hash = ((EhInt *) argv[0])->value;
    ehchar_hash(rt, &state, (EhChar *) argv[1]);
    return new_int(rt, NULL, state.hash);
}

EhValue *ehintr_int_add(EhRt *rt, EhValue **data, EhValue **argv) {
    EhInt *lhs = (EhInt *) argv[0];
    EhInt *rhs = (EhInt *) argv[1];
    return new_int(rt, NULL, lhs->value + rhs->value);
}

EhValue *ehintr_ratio_add(EhRt *rt, EhValue **data, EhValue **argv) {
    EhRatio *lhs = (EhRatio *) argv[0];
    EhRatio *rhs = (EhRatio *) argv[1];
    return ehratio_add(rt, lhs, rhs);
}

EhValue *ehintr_real_add(EhRt *rt, EhValue **data, EhValue **argv) {
    EhReal *lhs = (EhReal *) argv[0];
    EhReal *rhs = (EhReal *) argv[1];
    return new_real(rt, NULL, lhs->value + rhs->value);
}

EhValue *ehintr_int_sub(EhRt *rt, EhValue **data, EhValue **argv) {
    EhInt *lhs = (EhInt *) argv[0];
    EhInt *rhs = (EhInt *) argv[1];
    return new_int(rt, NULL, lhs->value - rhs->value);
}

EhValue *ehintr_ratio_sub(EhRt *rt, EhValue **data, EhValue **argv) {
    EhRatio *lhs = (EhRatio *) argv[0];
    EhRatio *rhs = (EhRatio *) argv[1];
    return ehratio_sub(rt, lhs, rhs);
}

EhValue *ehintr_real_sub(EhRt *rt, EhValue **data, EhValue **argv) {
    EhReal *lhs = (EhReal *) argv[0];
    EhReal *rhs = (EhReal *) argv[1];
    return new_real(rt, NULL, lhs->value - rhs->value);
}

EhValue *ehintr_int_mul(EhRt *rt, EhValue **data, EhValue **argv) {
    EhInt *lhs = (EhInt *) argv[0];
    EhInt *rhs = (EhInt *) argv[1];
    return new_int(rt, NULL, lhs->value * rhs->value);
}

EhValue *ehintr_ratio_mul(EhRt *rt, EhValue **data, EhValue **argv) {
    EhRatio *lhs = (EhRatio *) argv[0];
    EhRatio *rhs = (EhRatio *) argv[1];
    return ehratio_mul(rt, lhs, rhs);
}

EhValue *ehintr_real_mul(EhRt *rt, EhValue **data, EhValue **argv) {
    EhReal *lhs = (EhReal *) argv[0];
    EhReal *rhs = (EhReal *) argv[1];
    return new_real(rt, NULL, lhs->value * rhs->value);
}

EhValue *ehintr_int_div(EhRt *rt, EhValue **data, EhValue **argv) {
    EhInt *lhs = (EhInt *) argv[0];
    EhInt *rhs = (EhInt *) argv[1];
    return new_int(rt, NULL, lhs->value / rhs->value);
}

EhValue *ehintr_ratio_div(EhRt *rt, EhValue **data, EhValue **argv) {
    EhRatio *lhs = (EhRatio *) argv[0];
    EhRatio *rhs = (EhRatio *) argv[1];
    return ehratio_div(rt, lhs, rhs);
}

EhValue *ehintr_real_div(EhRt *rt, EhValue **data, EhValue **argv) {
    EhReal *lhs = (EhReal *) argv[0];
    EhReal *rhs = (EhReal *) argv[1];
    return new_real(rt, NULL, lhs->value / rhs->value);
}

EhValue *ehintr_int_mod(EhRt *rt, EhValue **data, EhValue **argv) {
    EhInt *lhs = (EhInt *) argv[0];
    EhInt *rhs = (EhInt *) argv[1];
    return new_int(rt, NULL, lhs->value % rhs->value);
}

EhValue *ehintr_list_head(EhRt *rt, EhValue **data, EhValue **argv) {
    EhList *lst = (EhList *) *argv;
    assert_eq(rt, lst->variant, EH_LIST_CONS, "cannot take the head of ()");
    return ((EhListCons *) lst)->head;
}

EhValue *ehintr_list_tail(EhRt *rt, EhValue **data, EhValue **argv) {
    EhList *lst = (EhList *) *argv;
    assert_eq(rt, lst->variant, EH_LIST_CONS, "cannot take the tail of ()");
    return (EhValue *) ((EhListCons *) lst)->tail;
}

EhValue *ehintr_var_get(EhRt *rt, EhValue **data, EhValue **argv) {
    return ((EhVar *) argv[0])->inner;
}

EhValue *ehintr_var_update(EhRt *rt, EhValue **data, EhValue **argv) {
    ((EhVar *) argv[0])->inner = argv[1];
    return new_nil(rt, NULL);
}

EhValue *ehintr_new_nil(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_nil(rt, NULL);
}

EhValue *ehintr_new_atom_id(EhRt *rt, EhValue **data, EhValue **argv) {
    EhAtom *atom = (EhAtom *) *argv;
    return new_atom(rt, NULL, atom->size - sizeof(EhAtom), atom->atom);
}

EhValue *ehintr_new_bool_true(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_bool(rt, NULL, 1);
}

EhValue *ehintr_new_bool_false(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_bool(rt, NULL, 0);
}

EhValue *ehintr_new_int_id(EhRt *rt, EhValue **data, EhValue **argv) {
    EhInt *value = (EhInt *) *argv;
    return new_int(rt, NULL, value->value);
}

EhValue *ehintr_new_ratio_ii(EhRt *rt, EhValue **data, EhValue **argv) {
    long p = ((EhInt *) argv[0])->value;
    long q = ((EhInt *) argv[1])->value;
    enum EhRatioSign sign = (p < 0) ^ (q < 0);
    return new_ratio(rt, NULL, sign, labs(p), labs(q));
}

EhValue *ehintr_new_real_id(EhRt *rt, EhValue **data, EhValue **argv) {
    EhReal *value = (EhReal *) *argv;
    return new_real(rt, NULL, value->value);
}

EhValue *ehintr_new_char_id(EhRt *rt, EhValue **data, EhValue **argv) {
    EhChar *value = (EhChar *) *argv;
    return new_char(rt, NULL, value->value);
}

EhValue *ehintr_new_list_empty(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_list_empty(rt, NULL);
}

EhValue *ehintr_new_list_cons(EhRt *rt, EhValue **data, EhValue **argv) {
    EhValue *elem = argv[0];
    EhValue *list = argv[1];
    return new_list_cons(rt, NULL, elem, list);
}

EhValue *ehintr_new_var_any(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_var(rt, NULL, argv[0]);
}

EhValue *ehintr_nil_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehnil_repr(rt, (EhNil *) argv[0]);
}

EhValue *ehintr_atom_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehatom_repr(rt, (EhAtom *) argv[0]);
}

EhValue *ehintr_bool_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehbool_repr(rt, (EhBool *) argv[0]);
}

EhValue *ehintr_int_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehint_repr(rt, (EhInt *) argv[0]);
}

EhValue *ehintr_ratio_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehratio_repr(rt, (EhRatio *) argv[0]);
}

EhValue *ehintr_real_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehreal_repr(rt, (EhReal *) argv[0]);
}

EhValue *ehintr_char_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehchar_repr(rt, (EhChar *) argv[0]);
}

EhValue *ehintr_list_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehlist_repr(rt, (EhList *) argv[0]);
}

EhValue *ehintr_bind_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehbind_repr(rt, (EhBind *) argv[0]);
}

EhValue *ehintr_destr_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehdestr_repr(rt, (EhDestr *) argv[0]);
}

EhValue *ehintr_closure_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehclosure_repr(rt, (EhClosure *) argv[0]);
}

EhValue *ehintr_var_repr(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehvar_repr(rt, (EhVar *) argv[0]);
}

EhValue *ehintr_nil_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehnil_display(rt, (EhNil *) argv[0]);
}

EhValue *ehintr_atom_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehatom_display(rt, (EhAtom *) argv[0]);
}

EhValue *ehintr_bool_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehbool_display(rt, (EhBool *) argv[0]);
}

EhValue *ehintr_int_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehint_display(rt, (EhInt *) argv[0]);
}

EhValue *ehintr_ratio_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehratio_display(rt, (EhRatio *) argv[0]);
}

EhValue *ehintr_real_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehreal_display(rt, (EhReal *) argv[0]);
}

EhValue *ehintr_char_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehchar_display(rt, (EhChar *) argv[0]);
}

EhValue *ehintr_list_display(EhRt *rt, EhValue **data, EhValue **argv) {
    return ehlist_display(rt, (EhList *) argv[0]);
}

EhValue *ehintr_int_to_ratio(EhRt *rt, EhValue **data, EhValue **argv) {
    int value = ((EhInt *) argv[0])->value;
    enum EhRatioSign sign = value < 0;
    return new_ratio(rt, NULL, sign, labs(value), 1);
}

EhValue *ehintr_int_to_real(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_real(rt, NULL, ((EhInt *) argv[0])->value);
}

EhValue *ehintr_int_to_char(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_char(rt, NULL, ((EhInt *) argv[0])->value);
}

EhValue *ehintr_ratio_to_int(EhRt *rt, EhValue **data, EhValue **argv) {
    enum EhRatioSign sign = ((EhRatio *) argv[0])->sign;
    unsigned long p = ((EhRatio *) argv[0])->p;
    unsigned long q = ((EhRatio *) argv[0])->q;
    long value = sign? -p/q : p/q;
    return new_int(rt, NULL, value);
}

EhValue *ehintr_ratio_to_real(EhRt *rt, EhValue **data, EhValue **argv) {
    enum EhRatioSign sign = ((EhRatio *) argv[0])->sign;
    unsigned long p = ((EhRatio *) argv[0])->p;
    unsigned long q = ((EhRatio *) argv[0])->q;
    double value = sign? -(double) p/(double) q : (double) p/(double) q;
    return new_real(rt, NULL, value);
}

EhValue *ehintr_real_to_int(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, trunc(((EhReal *) argv[0])->value));
}

EhValue *ehintr_real_to_ratio(EhRt *rt, EhValue **data, EhValue **argv) {
    double value = ((EhReal *) argv[0])->value;
    unsigned long q = 1;
    double intpart;
    double fract = modf(value, &intpart);
    while (fabs(fract) > __DBL_EPSILON__) {
        fract = modf(value, &intpart);
        fract *= 10.0;
        q *= 10;
    }
    long p = (long) intpart;
    enum EhRatioSign sign = p < 0;
    return new_ratio(rt, NULL, sign, labs(p), q);
}

EhValue *ehintr_char_to_int(EhRt *rt, EhValue **data, EhValue **argv) {
    return new_int(rt, NULL, ((EhChar *) argv[0])->value);
}
