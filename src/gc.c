#include <stdlib.h>
#include "value.h"
#include "panic.h"
#include "gc.h"
#include "alloc.h"

EhGc *new_gc(EhRt *rt, EhGc *gc, size_t capacity) {
    assert(rt, capacity > 0, "gc capacity cannot be zero");

    if (!gc) {
        gc = malloc(sizeof(EhGc));
    }
    
    gc->capacity = capacity;
    gc->size = 0;
    gc->allocations = 0;
    gc->allocations_since = 0;
    gc->enabled = 1;
    gc->vl.type = EH_NIL;
    gc->vl.mark = 0;
    gc->vl.size = sizeof(EhNil);
    gc->vl.next = NULL;
    new_allocator(&gc->alloc, capacity);

    return gc;
}

void del_gc(EhRt *rt, EhGc *gc) {
    del_allocator(&gc->alloc);
}

EhValue *ehgc_alloc(EhRt *rt, EhGc *gc, const enum TypeId type, const size_t size) {
    assert(rt, gc, "gc is NULL");

    if (ehgc_should_collect(rt, gc)) {
        ehgc_collect(rt, gc, rt->stack, rt->stack_len);
    }

    EhValue *value;
    value = ehalloc(&gc->alloc, size);
    assert(rt, value, "oom");
    value->type = type;
    value->size = size;
    value->mark = 0;
    value->next = gc->vl.next;
    gc->vl.next = value;

    size_t real_size = ((size - 1) / sizeof(size_t) + 1) * sizeof(size_t);
    gc->size += real_size;
    ++gc->allocations;
    ++gc->allocations_since;

    return value;
}

void ehgc_enable(EhRt *rt, EhGc *gc) {
    gc->enabled = 1;
}

void ehgc_disable(EhRt *rt, EhGc *gc) {
    gc->enabled = 0;
}

static void mark(EhRt *rt, EhValue *value) {
    assert(rt, value, "attempt to mark NULL");
    if (value->mark) {
        return;
    }
    value->mark = 1;

    size_t len;
    switch (value->type) {
        case EH_NIL: return;
        case EH_ATOM: return;
        case EH_BOOL: return;
        case EH_INT: return;
        case EH_REAL: return;
        case EH_CHAR: return;
        case EH_LIST:
            switch (((EhList *) value)->variant) {
                case EH_LIST_EMPTY: return;
                case EH_LIST_CONS:
                    mark(rt, ((EhListCons *) value)->head);
                    mark(rt, (EhValue *) ((EhListCons *) value)->tail);
                    return;
            }
            return;
        case EH_BIND: return;
        case EH_DESTR:
            len = (value->size - sizeof(EhDestr)) / sizeof(EhValue *);
            for (size_t i = 0; i < len; i++) {
                mark(rt, ((EhDestr *) value)->patterns[i]);
            }
            return;
        case EH_HNODE:
            switch (((EhHnode *) value)->variant) {
                case EH_HTRIE_LEAF:
                    mark(rt, ((EhHleaf *) value)->key);
                    mark(rt, ((EhHleaf *) value)->value);
                    return;
                case EH_HTRIE_BRANCH:
                    for (size_t i = 0; i < EH_HTRIE_NODES; i++) {
                        EhHnode *node;
                        if ((node = ((EhHbranch *) value)->nodes[i])) {
                            mark(rt, (EhValue *) node);
                        }
                    }
                    return;
            }
            return;
        case EH_HTRIE:
            if (((EhHtrie *) value)->root) {
                mark(rt, (EhValue *) ((EhHtrie *) value)->root);
            }
            return;
        case EH_FUNCTION:
            len = ((EhFunction *) value)->paramc + ((EhFunction *) value)->datac;
            mark(rt, (EhValue *) ((EhFunction *) value)->env);
            for (size_t i = 0; i < len; i++) {
                mark(rt, ((EhFunction *) value)->data[i]);
            }
            return;
        case EH_IFUNC:
            len = ((EhFunction *) value)->paramc + ((EhFunction *) value)->datac;
            mark(rt, (EhValue *) ((EhFunction *) value)->env);
            for (size_t i = 0; i < len; i++) {
                mark(rt, ((EhIFunc *) value)->data[i]);
            }
            return;
        case EH_CLOSURE:
            len = (value->size - sizeof(EhClosure)) / sizeof(EhValue *);
            for (size_t i = 0; i < len; i++) {
                mark(rt, ((EhClosure *) value)->implv[i]);
            }
            return;
        case EH_VAR:
            mark(rt, ((EhVar *) value)->inner);
            return;
        default:
            len = (value->size - sizeof(EhData)) / sizeof(EhValue *);
            for (size_t i = 0; i < len; i++) {
                mark(rt, ((EhData *) value)->fieldv[i]);
            }
            return;
    }
}

static void markall(EhRt *rt, EhGc *gc, EhValue **stack, const size_t stack_len) {
    assert(rt, gc, "gc is NULL");
    assert(rt, stack, "stack is NULL");

    for (size_t i = 0; i < stack_len; i++) {
        mark(rt, stack[i]);
    }
}

static void sweep(EhRt *rt, EhGc *gc) {
    assert(rt, gc, "gc is NULL");

    EhValue *previous = &gc->vl;
    EhValue *current = gc->vl.next;
    while (current) {
        if (!current->mark) {
            size_t size = ((current->size - 1) / sizeof(size_t) + 1) * sizeof(size_t);
            previous->next = current->next;
            gc->size -= size;
            --gc->allocations;
            ehfree(&gc->alloc, current);
            current = previous->next;
        } else {
            previous = current;
            current = current->next;
        }
    }
}

static void unmarkall(EhRt *rt, EhGc *gc) {
    assert(rt, gc, "gc is NULL");

    EhValue *previous = &gc->vl;
    EhValue *current = gc->vl.next;
    while (current) {
        current->mark = 0;
        previous = current;
        current = current->next;
    }
}

void ehgc_collect(EhRt *rt, EhGc *gc, EhValue **stack, const size_t stack_len) {
    assert(rt, gc, "gc is NULL");
    assert(rt, stack, "stack is NULL");

    gc->allocations_since = 0;

    markall(rt, gc, stack, stack_len);
    sweep(rt, gc);
    unmarkall(rt, gc);
}

int ehgc_should_collect(EhRt *rt, EhGc *gc) {
    assert(rt, gc, "gc is NULL");
    if (!gc->enabled) {
        return 0;
    }
    return gc->size > (gc->capacity / 10 * 9);
}
