#include <stdio.h>
#include <string.h>
#include "rt.h"
#include "gc.h"
#include "value.h"
#include "array.h"
#include "intrinsic.h"
#include "panic.h"
#include "types.h"
#include "cmath.h"
#include "sys.h"

EhRt *new_rt(EhRt *rt, const EhInfo *info) {
    if (!rt) {
        rt = malloc(sizeof(EhRt));
    }

    new_gc(rt, &rt->gc, info->heap);
    new_array(rt, &rt->argv, info->argc, sizeof(char *));
    memcpy(rt->argv.buffer, info->argv, info->argc * sizeof(char *));
    rt->argv.len = info->argc;
    rt->interp = NULL;
    rt->interp_call = NULL;

    rt->call_stack = malloc(info->call_stack * sizeof(void *));
    rt->csp = 0;
    rt->csc = info->call_stack / sizeof(void *);

    rt->types = malloc(MAX_TYPES * sizeof(EhType *));
    rt->types_len = 0;
    memset(rt->types, 0, MAX_TYPES * sizeof(EhType *));

    rt->stack = malloc(info->stack * sizeof(EhValue *));
    rt->stack_len = 0;
    rt->stack_cap = info->stack;

    rt->bp = 0;
    rt->frames = malloc(info->frames * sizeof(size_t));
    rt->frames_len = 0;
    rt->frames_cap = info->frames;

    rt->thread_id = info->thread_id;

    ehrt_disable_gc(rt);
    ehrt_push(rt, new_env(rt));
    init_lib_cmath(rt);
    init_lib_sys(rt);
    ehrt_enable_gc(rt);

    rt->except.panicking = 0;
    rt->except.sp = rt->stack_len;
    rt->except.bp = rt->bp;
    rt->except.fp = rt->frames_len;

    new_debug_data(&rt->debug, 0, 128, NULL);

    return rt;
}

EhValue *new_env(EhRt *rt) {
    EhHtrie *scope = new_htrie(rt);
    EhHtrie *env = scope;
    EhHtrie *intrinsic = new_htrie(rt);

#define BINARY_ARITHMETIC(name, iimpl, qimpl, fimpl) \
    { \
        EhValue *i_paramv[] = { new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL) }; \
        EhValue *q_paramv[] = { new_bind(rt, NULL, EH_RATIO, NULL), new_bind(rt, NULL, EH_RATIO, NULL) }; \
        EhValue *f_paramv[] = { new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL) }; \
        EhFunction *implv[] = { \
            (EhFunction *) new_function(rt, NULL, iimpl, scope, 2, i_paramv, 0, NULL), \
            (EhFunction *) new_function(rt, NULL, qimpl, scope, 2, q_paramv, 0, NULL), \
            (EhFunction *) new_function(rt, NULL, fimpl, scope, 2, f_paramv, 0, NULL), \
        }; \
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, name); \
        EhValue *closure = new_closure(rt, NULL, 0, 3, implv); \
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure); \
    }

    BINARY_ARITHMETIC("add", ehintr_int_add, ehintr_ratio_add, ehintr_real_add);
    BINARY_ARITHMETIC("sub", ehintr_int_sub, ehintr_ratio_sub, ehintr_real_sub);
    BINARY_ARITHMETIC("mul", ehintr_int_mul, ehintr_ratio_mul, ehintr_real_mul);
    BINARY_ARITHMETIC("div", ehintr_int_div, ehintr_ratio_div, ehintr_real_div);
    {
        EhValue *i_paramv[] = { new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL) }; \
        EhFunction *implv[] = { \
            (EhFunction *) new_function(rt, NULL, ehintr_int_mod, scope, 2, i_paramv, 0, NULL), \
        }; \
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "mod"); \
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv); \
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure); \
    }
    BINARY_ARITHMETIC("lt", ehintr_int_lt, ehintr_ratio_lt, ehintr_real_lt);
    BINARY_ARITHMETIC("gt", ehintr_int_gt, ehintr_ratio_gt, ehintr_real_gt);
    BINARY_ARITHMETIC("le", ehintr_int_le, ehintr_ratio_le, ehintr_real_le);
    BINARY_ARITHMETIC("ge", ehintr_int_ge, ehintr_ratio_ge, ehintr_real_ge);

    {
        EhValue *nil_paramv[] = { new_bind(rt, NULL, EH_NIL, NULL) };
        EhValue *bool_paramv[] = { new_bind(rt, NULL, EH_BOOL, NULL) };
        EhValue *int_paramv[] = { new_bind(rt, NULL, EH_INT, NULL) };
        EhValue *ratio_paramv[] = { new_bind(rt, NULL, EH_RATIO, NULL) };
        EhValue *real_paramv[] = { new_bind(rt, NULL, EH_REAL, NULL) };
        EhValue *char_paramv[] = { new_bind(rt, NULL, EH_CHAR, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_nil_hash, scope, 1, nil_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_bool_hash, scope, 1, bool_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_int_hash, scope, 1, int_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_ratio_hash, scope, 1, ratio_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_real_hash, scope, 1, real_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_char_hash, scope, 1, char_paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "hash");
        EhValue *closure = new_closure(rt, NULL, 0, 6, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

    {
        EhValue *nil_paramv[] = { new_bind(rt, NULL, EH_NIL, NULL), new_bind(rt, NULL, EH_NIL, NULL) };
        EhValue *bool_paramv[] = { new_bind(rt, NULL, EH_BOOL, NULL), new_bind(rt, NULL, EH_BOOL, NULL) };
        EhValue *int_paramv[] = { new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL) };
        EhValue *ratio_paramv[] = { new_bind(rt, NULL, EH_RATIO, NULL), new_bind(rt, NULL, EH_RATIO, NULL) };
        EhValue *real_paramv[] = { new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL) };
        EhValue *char_paramv[] = { new_bind(rt, NULL, EH_CHAR, NULL), new_bind(rt, NULL, EH_CHAR, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_nil_eq, scope, 2, nil_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_bool_eq, scope, 2, bool_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_int_eq, scope, 2, int_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_ratio_eq, scope, 2, ratio_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_real_eq, scope, 2, real_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_char_eq, scope, 2, char_paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "eq");
        EhValue *closure = new_closure(rt, NULL, 0, 6, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

    {
        EhValue *nil_paramv[] = { new_bind(rt, NULL, EH_NIL, NULL), new_bind(rt, NULL, EH_NIL, NULL) };
        EhValue *bool_paramv[] = { new_bind(rt, NULL, EH_BOOL, NULL), new_bind(rt, NULL, EH_BOOL, NULL) };
        EhValue *int_paramv[] = { new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL) };
        EhValue *ratio_paramv[] = { new_bind(rt, NULL, EH_RATIO, NULL), new_bind(rt, NULL, EH_RATIO, NULL) };
        EhValue *real_paramv[] = { new_bind(rt, NULL, EH_REAL, NULL), new_bind(rt, NULL, EH_REAL, NULL) };
        EhValue *char_paramv[] = { new_bind(rt, NULL, EH_CHAR, NULL), new_bind(rt, NULL, EH_CHAR, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_nil_ne, scope, 2, nil_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_bool_ne, scope, 2, bool_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_int_ne, scope, 2, int_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_ratio_ne, scope, 2, ratio_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_real_ne, scope, 2, real_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_char_ne, scope, 2, char_paramv, 0, NULL),
        }; \
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "ne");
        EhValue *closure = new_closure(rt, NULL, 0, 6, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

#undef BINARY_ARITHMETIC

#define UNARY_LIST(name, impl) \
    { \
        EhValue *paramv[] = { new_bind(rt, NULL, EH_LIST, NULL) }; \
        EhFunction *implv[] = { \
            (EhFunction *) new_function(rt, NULL, impl, scope, 1, paramv, 0, NULL), \
        }; \
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, name); \
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv); \
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure); \
    }

    UNARY_LIST("head", ehintr_list_head);
    UNARY_LIST("tail", ehintr_list_tail);

#undef UNARY_LIST

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_VAR, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_var_get, scope, 1, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "getvar");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_VAR, NULL), new_bind(rt, NULL, EH_ANY, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_var_update, scope, 2, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "updatevar");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

    {
        EhValue *nil_paramv[] = { new_bind(rt, NULL, EH_NIL, NULL) };
        EhValue *atom_paramv[] = { new_bind(rt, NULL, EH_ATOM, NULL) };
        EhValue *bool_paramv[] = { new_bind(rt, NULL, EH_BOOL, NULL) };
        EhValue *int_paramv[] = { new_bind(rt, NULL, EH_INT, NULL) };
        EhValue *ratio_paramv[] = { new_bind(rt, NULL, EH_RATIO, NULL) };
        EhValue *real_paramv[] = { new_bind(rt, NULL, EH_REAL, NULL) };
        EhValue *char_paramv[] = { new_bind(rt, NULL, EH_CHAR, NULL) };
        EhValue *list_paramv[] = { new_bind(rt, NULL, EH_LIST, NULL) };
        EhValue *bind_paramv[] = { new_bind(rt, NULL, EH_BIND, NULL) };
        EhValue *destr_paramv[] = { new_bind(rt, NULL, EH_DESTR, NULL) };
        EhValue *closure_paramv[] = { new_bind(rt, NULL, EH_CLOSURE, NULL) };
        EhValue *var_paramv[] = { new_bind(rt, NULL, EH_VAR, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_nil_repr, scope, 1, nil_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_atom_repr, scope, 1, atom_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_bool_repr, scope, 1, bool_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_int_repr, scope, 1, int_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_ratio_repr, scope, 1, ratio_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_real_repr, scope, 1, real_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_char_repr, scope, 1, char_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_list_repr, scope, 1, list_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_bind_repr, scope, 1, bind_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_destr_repr, scope, 1, destr_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_closure_repr, scope, 1, closure_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_var_repr, scope, 1, var_paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "repr");
        EhValue *closure = new_closure(rt, NULL, 0, 12, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

    {
        EhValue *nil_paramv[] = { new_bind(rt, NULL, EH_NIL, NULL) };
        EhValue *atom_paramv[] = { new_bind(rt, NULL, EH_ATOM, NULL) };
        EhValue *bool_paramv[] = { new_bind(rt, NULL, EH_BOOL, NULL) };
        EhValue *int_paramv[] = { new_bind(rt, NULL, EH_INT, NULL) };
        EhValue *ratio_paramv[] = { new_bind(rt, NULL, EH_RATIO, NULL) };
        EhValue *real_paramv[] = { new_bind(rt, NULL, EH_REAL, NULL) };
        EhValue *char_paramv[] = { new_bind(rt, NULL, EH_CHAR, NULL) };
        EhValue *list_paramv[] = { new_bind(rt, NULL, EH_LIST, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_nil_display, scope, 1, nil_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_atom_display, scope, 1, atom_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_bool_display, scope, 1, bool_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_int_display, scope, 1, int_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_ratio_display, scope, 1, ratio_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_real_display, scope, 1, real_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_char_display, scope, 1, char_paramv, 0, NULL),
            (EhFunction *) new_function(rt, NULL, ehintr_list_display, scope, 1, list_paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "display");
        EhValue *closure = new_closure(rt, NULL, 0, 8, implv);
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure);
    }

    {
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_nil, scope, 0, NULL, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Nil");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_ATOM, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_atom_id, scope, 1, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Atom");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_bool_true, scope, 0, NULL, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "True");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_bool_false, scope, 0, NULL, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "False");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_INT, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_int_id, scope, 1, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Int");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_INT, NULL), new_bind(rt, NULL, EH_INT, NULL)  };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_ratio_ii, scope, 2, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Ratio");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_REAL, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_real_id, scope, 1, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Real");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_CHAR, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_char_id, scope, 1, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Char");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_list_empty, scope, 0, NULL, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Empty");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_ANY, NULL), new_bind(rt, NULL, EH_LIST, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_list_cons, scope, 2, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Cons");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }


    {
        EhValue *paramv[] = { new_bind(rt, NULL, EH_ANY, NULL) };
        EhFunction *implv[] = {
            (EhFunction *) new_function(rt, NULL, ehintr_new_var_any, scope, 1, paramv, 0, NULL),
        };
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "Var");
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv);
        env = ehhtrie_assoc(rt, env, (EhValue *) atom, closure);
    }

#define UNARY_CONV(name, impl, from) \
    { \
        EhValue *paramv[] = { new_bind(rt, NULL, from, NULL) }; \
        EhFunction *implv[] = { \
            (EhFunction *) new_function(rt, NULL, impl, scope, 1, paramv, 0, NULL), \
        }; \
        EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, name); \
        EhValue *closure = new_closure(rt, NULL, 0, 1, implv); \
        intrinsic = ehhtrie_assoc(rt, intrinsic, (EhValue *) atom, closure); \
    }

    UNARY_CONV("int->ratio", ehintr_int_to_ratio, EH_INT);
    UNARY_CONV("int->real", ehintr_int_to_real, EH_INT);
    UNARY_CONV("int->char", ehintr_int_to_char, EH_INT);
    UNARY_CONV("ratio->int", ehintr_ratio_to_int, EH_RATIO);
    UNARY_CONV("ratio->real", ehintr_ratio_to_real, EH_RATIO);
    UNARY_CONV("real->int", ehintr_real_to_int, EH_REAL);
    UNARY_CONV("real->ratio", ehintr_real_to_ratio, EH_REAL);
    UNARY_CONV("char->int", ehintr_char_to_int, EH_CHAR);

#undef UNARY_CONV

    EhAtom *atom = (EhAtom *) ehatom_from_cstr(rt, NULL, "intrinsic");
    env = ehhtrie_assoc(rt, env, (EhValue *) atom, (EhValue *) intrinsic);

    return (EhValue *) env;
}

void del_rt(EhRt *rt) {
    ehgc_collect(rt, &rt->gc, rt->stack, 0);
    del_gc(rt, &rt->gc);
    del_array(rt, &rt->argv);
    for (size_t i = 0; i < rt->types_len; i++) {
        if (rt->types[i]) {
            del_type(rt, rt->types[i]);
        }
    }
    free(rt->types);
    free(rt->stack);
    free(rt->frames);
}

void ehrt_register_interp(EhRt *rt, EhInterpreter *interp, EhICall interp_call, EhIImport interp_import) {
    rt->interp = interp;
    rt->interp_call = interp_call;
    rt->interp_import = interp_import;
}

void ehrt_set_debug(EhRt *rt, EhDebugData *data) {
    memcpy(&rt->debug, data, sizeof(EhDebugData));
}

void ehrt_push_debug(EhRt *rt, EhDebugFrame *frame) {
    ehdebug_push(&rt->debug, frame);
}

int ehrt_find_debug(EhRt *rt, void *ptr, EhDebugFrame *frame) {
    return ehdebug_find(&rt->debug, ptr, frame);
}

void ehrt_enable_gc(EhRt *rt) {
    ehgc_enable(rt, &rt->gc);
}

void ehrt_disable_gc(EhRt *rt) {
    ehgc_disable(rt, &rt->gc);
}

void ehrt_enter(EhRt *rt, EhHtrie *env) {
    rt->frames[rt->frames_len] = rt->bp;
    ++rt->frames_len;
    rt->bp = rt->stack_len;
    ehrt_push(rt, (EhValue *) env);
}

void ehrt_leave(EhRt *rt) {
    rt->stack_len = rt->bp;
    --rt->frames_len;
    rt->bp = rt->frames[rt->frames_len];
}

void ehrt_newtype(EhRt *rt, EhType *type, unsigned int type_id) {
    assert(rt, !rt->types[type_id], "a type with this id already exists");
    ++type->rc;
    type->type_id = type_id;
    rt->types[type_id] = type;
    if (rt->types_len <= type_id) {
        rt->types_len = type_id + 1;
    }
}

void ehrt_push(EhRt *rt, EhValue *value) {
    assert(rt, rt->stack_len < rt->stack_cap, "stack overflow");
    rt->stack[rt->stack_len] = value;
    ++rt->stack_len;
}

EhValue *ehrt_pop(EhRt *rt) {
    assert(rt, rt->stack_len > 0, "stack underflow");
    --rt->stack_len;
    return rt->stack[rt->stack_len];
}

EhValue *ehrt_peek(EhRt *rt, ptrdiff_t idx) {
    assert(rt, rt->bp + idx < rt->stack_len, "index out of bounds");
    return rt->stack[rt->bp + idx];
}

EhValue *ehrt_top(EhRt *rt, size_t idx) {
    return ehrt_peek(rt, rt->stack_len - rt->bp - idx - 1);
}

EhValue *ehrt_get(EhRt *rt, EhAtom *key) {
    EhHtrie *scope = (EhHtrie *) ehrt_peek(rt, 0);
    return ehhtrie_find(rt, scope, (EhValue *) key);
}

void ehrt_pushcall(EhRt *rt, EhClosure *ptr, void *func, EhValue **out, int argc, jmp_buf *buf) {
    assert(rt, rt->csp < rt->csc, "call stack overflow");
    rt->call_stack[rt->csp].ptr = ptr;
    memcpy(rt->call_stack[rt->csp].jmp, buf, sizeof(jmp_buf));
    rt->call_stack[rt->csp].out = out;
    rt->call_stack[rt->csp].func = func;
    rt->call_stack[rt->csp].sp = rt->stack_len - argc;
    rt->call_stack[rt->csp].bp = rt->bp;
    rt->call_stack[rt->csp].fp = rt->frames_len;
    rt->call_stack[rt->csp].argc = argc;
    ++rt->csp;
}

EhClosure *ehrt_popcall(EhRt *rt, struct EhCallFrame *buf) {
    assert(rt, rt->csp > 0, "call stack underflow");
    --rt->csp;
    if (buf) {
        memcpy(buf, &rt->call_stack[rt->csp], sizeof(struct EhCallFrame));
    }
    rt->stack_len = rt->call_stack[rt->csp].sp;
    rt->bp = rt->call_stack[rt->csp].bp;
    rt->frames_len = rt->call_stack[rt->csp].fp;
    return rt->call_stack[rt->csp].ptr;
}

void ehrt_set(EhRt *rt, EhAtom *key, EhValue *value) {
    EhHtrie *scope = (EhHtrie *) ehrt_peek(rt, 0);
    EhHtrie *new;
    new = ehhtrie_assoc(rt, scope, (EhValue *) key, value);
    rt->stack[rt->bp] = (EhValue *) new;
}

int ehrt_start(EhMain main, const EhInfo *info) {
    EhRt rt;
    new_rt(&rt, info);
    int err = ehrt_catch_panic(&rt, main, info->data);
    del_rt(&rt);
    return err;
}

int ehrt_catch_panic(EhRt *rt, EhCb cb, void *data) {
    struct EhExcept previous;
    memcpy(&previous, &rt->except, sizeof(struct EhExcept));
    previous.sp = rt->stack_len;
    previous.bp = rt->bp;
    previous.fp = rt->frames_len;
    int ex;
    if ((ex = setjmp(rt->except.jmp))) {
        rt->stack_len = previous.sp;
        rt->bp = previous.bp;
        rt->frames_len = previous.fp;
        memcpy(&rt->except, &previous, sizeof(struct EhExcept));
        return ex;
    }
    cb(rt, data);
    return 0;
}
