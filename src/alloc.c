#include <stdlib.h>
#include <stdio.h>
#include "alloc.h"
#include "value.h"

EhAllocator *new_allocator(EhAllocator *alloc, const size_t capacity) {
    if (!alloc) {
        alloc = malloc(sizeof(EhAllocator));
    }

    alloc->capacity = capacity;
    alloc->allocated = 0;
    alloc->first = malloc(capacity);
    alloc->holes.size = 0;
    alloc->holes.next = alloc->first;
    alloc->holes.next->size = capacity;
    alloc->holes.next->next = NULL;

    return alloc;
}

void del_allocator(EhAllocator *alloc) {
    free(alloc->first);
}

EhValue *ehalloc(EhAllocator *alloc, size_t size) {
    size = ((size - 1) / sizeof(size_t) + 1) * sizeof(size_t);
    EhValue *value = NULL;
    EhHoles *previous = &alloc->holes;
    EhHoles *hole = alloc->holes.next;
    while (hole) {
        if (hole->size >= size) {
            EhHoles *next;
            if (hole->size - size >= sizeof(EhHoles)) {
                next = (EhHoles *) (((size_t) hole) + size);
                size_t ns = hole->size - size;
                EhHoles *nn = hole->next;
                next->size = ns;
                next->next = nn;
            } else {
                next = hole->next;
            }
            previous->next = next;
            value = (EhValue *) hole;
            value->size = size;
            alloc->allocated += size;
            break;
        }
        previous = hole;
        hole = hole->next;
    }
    return value;
}

static void defrag(EhAllocator *alloc) {
    EhHoles *previous = &alloc->holes;
    EhHoles *hole = alloc->holes.next;
    while (hole) {
        if ((size_t) hole - ((size_t) previous + previous->size) <= sizeof(EhHoles)) {
            size_t size = (size_t) hole + hole->size - (size_t) previous;
            EhHoles *next = hole->next;
            previous->size = size;
            previous->next = next;
            hole = next;
            continue;
        }
        previous = hole;
        hole = hole->next;
    }
}

void ehfree(EhAllocator *alloc, EhValue *value) {
    size_t size = ((value->size - 1) / sizeof(size_t) + 1) * sizeof(size_t);
    EhHoles *freed = (EhHoles *) value;
    freed->size = size;

    EhHoles *previous = &alloc->holes;
    EhHoles *hole = alloc->holes.next;
    while (hole) {
        if (hole == freed) {
            fprintf(stderr, "double free");
            exit(1);
        }
        if (hole > freed) {
            freed->next = hole;
            previous->next = freed;
            alloc->allocated -= size;
            defrag(alloc);
            return;
        }
        previous = hole;
        hole = hole->next;
    }
    previous->next = freed;
    freed->next = NULL;
    alloc->allocated -= size;
    defrag(alloc);
}
