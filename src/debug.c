#include <string.h>
#include "debug.h"

void new_debug_frame(EhDebugFrame *dst, void *ptr, int line, int column, char *file, char *ident) {
    dst->ptr = ptr;
    dst->line = line;
    dst->column = column;
    dst->file = malloc(strlen(file) + 1);
    strcpy(dst->file, file);
    dst->ident = malloc(strlen(ident) + 1);
    strcpy(dst->ident, ident);
}

void new_debug_data(EhDebugData *dst, size_t framen, size_t framec, EhDebugFrame *framev) {
    dst->framen = framen;
    dst->framec = framec;
    dst->framev = malloc(framec * sizeof(EhDebugFrame));
    memcpy(dst->framev, framev, framen * sizeof(EhDebugFrame));
}

void del_debug_data(EhDebugData *dst) {
    free(dst->framev);
}

void ehdebug_push(EhDebugData *dst, EhDebugFrame *frame) {
    if (dst->framen == dst->framec) {
        dst->framec <<= 2;
        dst->framev = realloc(dst->framev, dst->framec * sizeof(EhDebugFrame));
    }
    memcpy(dst->framev + dst->framen, frame, sizeof(EhDebugFrame));
    ++dst->framen;
}

int ehdebug_find(EhDebugData *data, void *ptr, EhDebugFrame *frame) {
    for (size_t i = 0; i < data->framen; i++) {
        if (data->framev[i].ptr <= ptr) {
            memcpy(frame, data->framev + i, sizeof(EhDebugFrame));
            return 1;
        }
    }
    return 0;
}
