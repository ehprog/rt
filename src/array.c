#include <stdlib.h>
#include <string.h>
#include "panic.h"
#include "array.h"

EhArray *new_array(EhRt *rt, EhArray *dst, const size_t cap, const size_t elem_size) {
    if (!dst) {
        dst = malloc(sizeof(EhArray));
        assert(rt, dst, "oom");
    }
    dst->buffer = malloc(cap * elem_size);
    dst->cap = cap;
    dst->elem_size = elem_size;
    return dst;
}

void del_array(EhRt *rt, EhArray *dst) {
    assert(rt, dst, "NULL pointer access");
    free(dst->buffer);
}

void *eharray_index(EhRt *rt, EhArray *array, const size_t idx) {
    return ehslice_index(rt, (EhSlice *) array, idx);
}

EhSlice *eharray_slice(EhRt *rt, EhArray *array, EhSlice *dst, const size_t from, const size_t to) {
    return ehslice_slice(rt, (EhSlice *) array, dst, from, to);
}

EhSliceIter *eharray_iter(EhRt *rt, EhArray *array, EhSliceIter *dst) {
    return ehslice_iter(rt, (EhSlice *) array, dst);
}

void eharray_get(EhRt *rt, const EhArray *array, void *dst, const size_t idx) {
    return ehslice_get(rt, (EhSlice *) array, dst, idx);
}

void eharray_set(EhRt *rt, EhArray *array, const void *src, const size_t idx) {
    return ehslice_set(rt, (EhSlice *) array, src, idx);
}

void eharray_push(EhRt *rt, EhArray *array, const void *elem) {
    assert(rt, elem, "NULL pointer access");
    if (array->len == array->cap) {
        array->cap *= 2;
        array->buffer = realloc(array->buffer, array->cap);
        assert(rt, array->buffer, "oom");
    }
    memcpy(array->buffer + array->len * array->elem_size, elem, array->elem_size);
    ++array->len;
}

void eharray_pop(EhRt *rt, EhArray *array, void *dst) {
    assert(rt, dst, "NULL pointer access");
    --array->len;
    memcpy(dst, array->buffer + array->len * array->elem_size, array->elem_size);
}

void eharray_shrink(EhRt *rt, EhArray *array) {
    assert(rt, array, "NULL pointer access");
    array->cap = array->len;
    array->buffer = realloc(array->buffer, array->cap);
    assert(rt, array->buffer, "oom");
}


void *ehslice_index(EhRt *rt, EhSlice *slice, const size_t idx) {
    assert(rt, idx < slice->len, "index %lu out of bounds (len is %lu)", idx, slice->len);
    return slice->buffer + idx * slice->elem_size;
}

EhSlice *ehslice_slice(EhRt *rt, EhSlice *slice, EhSlice *dst, const size_t from, const size_t to) {
    if (!dst) {
        dst = malloc(sizeof(EhSlice));
        assert(rt, dst, "oom");
    }

    assert(rt, from < slice->len, "index %lu out of bounds (len is %lu)", from, slice->len);
    assert(rt, to < slice->len, "index %lu out of bounds (len is %lu)", to, slice->len);
    assert(rt, from <= to, "negative slice");

    dst->buffer = slice->buffer;
    dst->len = to - from;
    dst->elem_size = slice->elem_size;
    return dst;
}

EhSliceIter *ehslice_iter(EhRt *rt, EhSlice *slice, EhSliceIter *dst) {
    if (!dst) {
        dst = malloc(sizeof(EhSliceIter));
        assert(rt, dst, "oom");
    }

    dst->buffer = slice->buffer;
    dst->len = slice->len;
    dst->elem_size = slice->elem_size;
    return dst;
}

void ehslice_get(EhRt *rt, const EhSlice *slice, void *dst, const size_t idx) {
    assert(rt, dst, "NULL pointer access");
    assert(rt, idx < slice->len, "index %lu out of bounds (len is %lu)", idx, slice->len);
    memcpy(dst, slice->buffer + idx * slice->elem_size, slice->elem_size);
}

void ehslice_set(EhRt *rt, EhSlice *slice, const void *src, const size_t idx) {
    assert(rt, src, "NULL pointer access");
    assert(rt, idx < slice->len, "index %lu out of bounds (len is %lu)", idx, slice->len);
    memcpy(slice->buffer + idx * slice->elem_size, src, slice->elem_size);
}


int ehsliceiter_next(EhRt *rt, EhSliceIter *iter, void *dst) {
    assert(rt, dst, "NULL pointer access");
    if (iter->len > 0) {
        memcpy(dst, iter->buffer, iter->elem_size);
        iter->buffer += iter->elem_size;
        --iter->len;
        return 1;
    } else {
        return 0;
    }
}
