#ifndef HASH_H
#define HASH_H 1

#include <stdlib.h>
#include <stdint.h>

typedef long long EhHashCode;
typedef struct EhHasher EhHasher;

struct EhHasher {
    EhHashCode hash;
};

EhHasher *new_hasher(EhHasher *state);
EhHashCode ehhasher_finalize(EhHasher *state);

void ehhasher_update_byte_array(EhHasher *state, void const *ptr, size_t len);
void ehhasher_update_cstr(EhHasher *state, char const *str);

void ehhasher_update_u64(EhHasher *state, uint64_t u64);
void ehhasher_update_f64(EhHasher *state, double f64);

#endif /* ifndef HASH_H */
