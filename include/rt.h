#ifndef RT_H
#define RT_H 1

#ifndef MAX_TYPES
#define MAX_TYPES 131072
#endif /* MAX_TYPES */

typedef struct EhRt EhRt;

#include <stddef.h>
#include <setjmp.h>
#include "value.h"
#include "gc.h"
#include "array.h"
#include "hash_trie.h"
#include "types.h"
#include "debug.h"

typedef struct EhInterpreter EhInterpreter;

typedef struct {
    int thread_id;
    size_t call_stack;
    size_t stack;
    size_t frames;
    size_t heap;
    int argc;
    char **argv;
    void *data;
} EhInfo;

typedef EhValue *(*EhICall)(EhRt *, EhInterpreter *, EhIFunc *, int flags, size_t, EhValue **, size_t, EhValue **);
typedef EhValue *(*EhIImport)(EhRt *, EhInterpreter *, EhIFunc *);

struct EhExcept {
    int panicking;
    jmp_buf jmp;
    size_t sp;
    size_t bp;
    size_t fp;
};

struct EhCallFrame {
    EhClosure *ptr;
    void *func;
    jmp_buf jmp;
    EhValue **out;
    size_t sp;
    size_t bp;
    size_t fp;
    int argc;
};

struct EhRt {
    struct EhExcept except;

    EhInterpreter *interp;
    EhICall interp_call;
    EhIImport interp_import;
    EhGc gc;
    EhArray argv;

    struct EhCallFrame *call_stack;
    size_t csp;
    size_t csc;

    EhType **types;
    size_t types_len;

    EhValue **stack;
    size_t stack_len;
    size_t stack_cap;

    size_t bp;
    size_t *frames;
    size_t frames_len;
    size_t frames_cap;

    int thread_id;

    EhDebugData debug;
};

typedef void (*EhCb)(EhRt *, void *);
typedef void (*EhMain)(EhRt *, void *);

EhRt *new_rt(EhRt *rt, const EhInfo *info);
EhValue *new_env(EhRt *rt);
void del_rt(EhRt *rt);
void ehrt_register_interp(EhRt *rt, EhInterpreter *interp, EhICall interp_call, EhIImport interp_import);
void ehrt_set_debug(EhRt *rt, EhDebugData *data);
void ehrt_push_debug(EhRt *rt, EhDebugFrame *frame);
int ehrt_find_debug(EhRt *rt, void *ptr, EhDebugFrame *frame);
void ehrt_enable_gc(EhRt *rt);
void ehrt_disable_gc(EhRt *rt);
void ehrt_enter(EhRt *rt, EhHtrie *env);
void ehrt_leave(EhRt *rt);
void ehrt_newtype(EhRt *rt, EhType *type, unsigned int type_id);
void ehrt_push(EhRt *rt, EhValue *value);
EhValue *ehrt_pop(EhRt *rt);
EhValue *ehrt_peek(EhRt *rt, ptrdiff_t idx);
EhValue *ehrt_top(EhRt *rt, size_t idx);
EhValue *ehrt_get(EhRt *rt, EhAtom *key);
void ehrt_pushcall(EhRt *rt, EhClosure *ptr, void *func, EhValue **out, int argc, jmp_buf *buf);
EhClosure *ehrt_popcall(EhRt *rt, struct EhCallFrame *buf);
void ehrt_set(EhRt *rt, EhAtom *key, EhValue *value);
int ehrt_start(EhMain main, const EhInfo *info);
int ehrt_catch_panic(EhRt *rt, EhCb cb, void *data);

#endif /* ifndef RT_H */
