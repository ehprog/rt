#ifndef HASH_TRIE_H
#define HASH_TRIE_H 1

#include "rt.h"
#include "value.h"

#define EH_HTRIE_SHIFT ((unsigned int) 5)
#define EH_HTRIE_NODES ((unsigned long) (1 << EH_HTRIE_SHIFT))
#define EH_HTRIE_MASK  ((unsigned long) (EH_HTRIE_NODES - 1))

enum EhHnodeTag {
    EH_HTRIE_LEAF,
    EH_HTRIE_BRANCH,
};

typedef struct {
    VALUE_HEADER;
    enum EhHnodeTag variant;
} EhHnode;

typedef struct {
    VALUE_HEADER;
    enum EhHnodeTag variant;
    EhValue *key;
    EhValue *value;
} EhHleaf;

typedef struct {
    VALUE_HEADER;
    enum EhHnodeTag variant;
    EhHnode *nodes[EH_HTRIE_NODES];
} EhHbranch;

typedef struct {
    VALUE_HEADER;
    EhHnode *root;
    size_t len;
} EhHtrie;

EhHtrie *new_htrie(EhRt *rt);
EhHbranch *new_hbranch(EhRt *rt);
EhHleaf *new_hleaf(EhRt *rt, EhValue *key, EhValue *value);

EhHtrie *ehhtrie_assoc(EhRt *rt, EhHtrie *trie, EhValue *key, EhValue *value);
EhHtrie *ehhtrie_remove(EhRt *rt, EhHtrie *trie, EhValue *key);
EhValue *ehhtrie_find(EhRt *rt, EhHtrie *trie, EhValue *key);
int ehhtrie_contains(EhRt *rt, EhHtrie *trie, EhValue *key);

#endif /* ifndef HASH_TRIE_H */
