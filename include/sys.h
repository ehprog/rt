#ifndef SYS_H
#define SYS_H 1

#include "rt.h"
#include "value.h"

void init_lib_sys(EhRt *rt);

/* ffi */
EhValue *sys_null(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_ptr_offset(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_ptr_diff(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_cstr_to_string(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_string_to_cstr(EhRt *rt, EhValue **data, EhValue **argv);

/* stdlib.h */
EhValue *sys_exit(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_abort(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_system(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_rand(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_srand(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_randmax(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_malloc(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_calloc(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_free(EhRt *rt, EhValue **data, EhValue **argv);

/* stdio.h */
EhValue *sys_stdin(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_stdout(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_stderr(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_fopen(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fclose(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fflush(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_fread(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fgetc(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fgets(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fwrite(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fputc(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fputs(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_feof(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_ferror(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_fseek(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_ftell(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_remove(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_rename(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_seek_end(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_seek_cur(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_seek_set(EhRt *rt, EhValue **data, EhValue **argv);

/* string.h */
EhValue *sys_memchr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_memcmp(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_memcpy(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_memmove(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_memset(EhRt *rt, EhValue **data, EhValue **argv);

/* time.h */
EhValue *sys_clock(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_clocks_per_sec(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_time(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_localtime(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *sys_gmtime(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *sys_tm_sec(EhRt *rt, EhValue **data, EhValue **argv);         /* seconds,  range 0 to 59          */
EhValue *sys_tm_min(EhRt *rt, EhValue **data, EhValue **argv);         /* minutes, range 0 to 59           */
EhValue *sys_tm_hour(EhRt *rt, EhValue **data, EhValue **argv);        /* hours, range 0 to 23             */
EhValue *sys_tm_mday(EhRt *rt, EhValue **data, EhValue **argv);        /* day of the month, range 1 to 31  */
EhValue *sys_tm_mon(EhRt *rt, EhValue **data, EhValue **argv);         /* month, range 0 to 11             */
EhValue *sys_tm_year(EhRt *rt, EhValue **data, EhValue **argv);        /* The number of years since 1900   */
EhValue *sys_tm_wday(EhRt *rt, EhValue **data, EhValue **argv);        /* day of the week, range 0 to 6    */
EhValue *sys_tm_yday(EhRt *rt, EhValue **data, EhValue **argv);        /* day in the year, range 0 to 365  */
EhValue *sys_tm_isdst(EhRt *rt, EhValue **data, EhValue **argv);       /* daylight saving time             */

#endif /* SYS_H */
