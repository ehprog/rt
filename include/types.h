#ifndef TYPES_H
#define TYPES_H 1

#include <stdlib.h>
#include "value.h"

#define EHTYPE_VARIADIC 0x1
#define EHTYPE_UNION 0x2
#define EHTYPE_UNIONALL 0x4
#define EHTYPE_CLOSURE 0x8
#define EHTYPE_DYNAMIC 0x10
#define EHTYPE_MUTABLE 0x20
#define EHTYPE_MACRO 0x40

enum TypeVariant {
    EH_TYPE,
    EH_TYPE_ATOM,
};

#define TYPE_HEADER \
    size_t size; \
    int rc; \
    enum TypeVariant variant; \
    unsigned int type_id;

typedef struct EhType EhType;
typedef struct EhTypeAtom EhTypeAtom;
typedef struct EhTypeExpr EhTypeExpr;

#include "rt.h"

struct EhType {
    TYPE_HEADER;
    int flags;
    EhTypeAtom *first;
    EhTypeExpr *rest[0];
};

// an atomic, non-parametric type
struct EhTypeAtom {
    TYPE_HEADER;
    char name[0];
};

// union of Type, TypeAtom
struct EhTypeExpr {
    TYPE_HEADER;
};

EhType *new_type(EhRt *rt, EhType *type, int flags, EhTypeAtom *first, size_t restc, EhTypeExpr **restv);
void del_type(EhRt *rt, EhType *type);
EhTypeAtom *new_type_atom(EhRt *rt, EhTypeAtom *type, size_t len, char *name);
int ehtype_matches(EhRt *rt, EhType *type, EhValue *value);

#endif /* TYPES_H */
