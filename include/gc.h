#ifndef GC_H
#define GC_H 1

#include <stdlib.h>
#include "value.h"
#include "alloc.h"
#include "rt.h"

typedef struct {
    size_t capacity;
    size_t size;
    size_t allocations;
    size_t allocations_since;
    EhValue vl;
    EhAllocator alloc;
    int enabled;
} EhGc;

EhGc *new_gc(EhRt *rt, EhGc *gc, size_t capacity);
void del_gc(EhRt *rt, EhGc *gc);
EhValue *ehgc_alloc(EhRt *rt, EhGc *gc, const enum TypeId type, const size_t size);
void ehgc_enable(EhRt *rt, EhGc *gc);
void ehgc_disable(EhRt *rt, EhGc *gc);
void ehgc_collect(EhRt *rt, EhGc *gc, EhValue **stack, const size_t stack_len);
int ehgc_should_collect(EhRt *rt, EhGc *gc);

#endif /* ifndef GC_H */
