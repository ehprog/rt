#ifndef ALLOC_H
#define ALLOC_H 1

#include <stdlib.h>

typedef struct EhHoles EhHoles;

struct EhHoles {
    size_t size;
    EhHoles *next;
};

typedef struct {
    size_t capacity;
    size_t allocated;
    EhHoles *first;
    EhHoles holes;
} EhAllocator;

#include "value.h"

EhAllocator *new_allocator(EhAllocator *alloc, const size_t capacity);
void del_allocator(EhAllocator *alloc);
EhValue *ehalloc(EhAllocator *alloc, size_t size);
void ehfree(EhAllocator *alloc, EhValue *value);

#endif /* ifndef ALLOC_H */
