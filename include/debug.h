#ifndef DEBUG_H
#define DEBUG_H 1

#include <stdlib.h>

typedef struct EhDebugFrame EhDebugFrame;
typedef struct EhDebugData EhDebugData;

struct EhDebugFrame {
    void *ptr; // key
    int line; // line in file
    int column; // column in file
    char *file; // filename
    char *ident; // identifier
};

struct EhDebugData {
    size_t framen;
    size_t framec;
    EhDebugFrame *framev;
};

void new_debug_frame(EhDebugFrame *dst, void *ptr, int line, int column, char *file, char *ident);
void new_debug_data(EhDebugData *dst, size_t framen, size_t framec, EhDebugFrame *framev);
void del_debug_data(EhDebugData *dst);
void ehdebug_push(EhDebugData *dst, EhDebugFrame *frame);
int ehdebug_find(EhDebugData *data, void *ptr, EhDebugFrame *frame);

#endif /* DEBUG_H */
