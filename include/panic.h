#ifndef PANIC_H
#define PANIC_H 1

#include <stdlib.h>
#include <stdio.h>
#include "rt.h"

#define panic(rt, ...) { \
    fprintf(stderr, "thread %d panicked at %s:%d: `", rt->thread_id, __FILE__, __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "`\n"); \
    _panic(rt); \
}
#define explicit_panic(rt) panic(rt, "explicit panic")
#define unimplemented(rt) panic(rt, "not yet implemented")
#define unreachable(rt) panic(rt, "an unreachable branch was reached")
#define assert(rt, p, ...) if (!(p)) panic(rt, __VA_ARGS__)
#define assert_eq(rt, a, b, ...) if ((a) != (b)) panic(rt, __VA_ARGS__)

_Noreturn
void _panic(EhRt *rt);

#endif /* ifndef PANIC_H */
