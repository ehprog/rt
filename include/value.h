#ifndef VALUE_H
#define VALUE_H 1

#include <stdlib.h>
#include "hash.h"

#define VALUE_HEADER \
    enum TypeId type; \
    int mark; \
    size_t size; \
    EhValue *next;

#define HEADER_SIZE sizeof(EhValue)

#define EHCLOSURE_DYNAMIC 1
#define EHCLOSURE_MUTABLE 2

enum TypeId {
    EH_NIL = 0,
    EH_ANY = 1,
    EH_ATOM,
    EH_BOOL,
    EH_PTR,
    EH_INT,
    EH_RATIO,
    EH_REAL,
    EH_CHAR,
    EH_LIST,
    EH_BIND,
    EH_DESTR,
    EH_HNODE,
    EH_HTRIE,
    EH_FUNCTION,
    EH_IFUNC,
    EH_CLOSURE,
    EH_VAR,
    EH_LAST,
};

typedef struct EhValue EhValue;
typedef struct EhNil EhNil;
typedef struct EhAtom EhAtom;
typedef struct EhBool EhBool;
typedef struct EhPtr EhPtr;
typedef struct EhInt EhInt;
typedef struct EhReal EhReal;
typedef struct EhChar EhChar;
typedef struct EhList EhList;
typedef struct EhListEmpty EhListEmpty;
typedef struct EhListCons EhListCons;
typedef struct EhBind EhBind;
typedef struct EhDestr EhDestr;
typedef struct EhClosure EhClosure;
typedef struct EhFunction EhFunction;
typedef struct EhIFunc EhIFunc;
typedef struct EhVar EhVar;
typedef struct EhData EhData;

struct EhValue {
    VALUE_HEADER;
    char data[0];
};

struct EhNil {
    VALUE_HEADER;
};

struct EhAtom {
    VALUE_HEADER;
    char atom[0];
};

enum EhBoolTag {
    EH_BOOL_FALSE = 0,
    EH_BOOL_TRUE = 1,
};

struct EhBool {
    VALUE_HEADER;
    enum EhBoolTag value;
};

struct EhPtr {
    VALUE_HEADER;
    void *ptr;
};

struct EhInt {
    VALUE_HEADER;
    long value;
};

struct EhReal {
    VALUE_HEADER;
    double value;
};

struct EhChar {
    VALUE_HEADER;
    unsigned int value;
};

enum EhListTag {
    EH_LIST_EMPTY,
    EH_LIST_CONS,
};

struct EhList {
    VALUE_HEADER;
    enum EhListTag variant;
};

struct EhListEmpty {
    VALUE_HEADER;
    enum EhListTag variant;
};

struct EhListCons {
    VALUE_HEADER;
    enum EhListTag variant;
    EhValue *head;
    EhList *tail;
};

struct EhBind {
    VALUE_HEADER;
    unsigned int type_id;
    EhClosure *pred;
};

struct EhDestr {
    VALUE_HEADER;
    unsigned int type_id;
    unsigned int variant;
    EhValue *patterns[0];
};

#include "ratio.h"
#include "rt.h"
#include "hash_trie.h"

typedef EhValue *(*EhFunc)(EhRt *, EhValue **, EhValue **);

struct EhClosure {
    VALUE_HEADER;
    int flags;
    EhValue *implv[0];
};

struct EhFunction {
    VALUE_HEADER;
    EhFunc func;
    EhHtrie *env;
    unsigned int paramc;
    unsigned int datac;
    EhValue *data[0];
};

struct EhIFunc {
    VALUE_HEADER;
    size_t func;
    EhHtrie *env;
    unsigned int paramc;
    unsigned int datac;
    EhValue *data[0];
};

struct EhVar {
    VALUE_HEADER;
    EhValue *inner;
};

struct EhData {
    VALUE_HEADER;
    unsigned int variant;
    EhValue *fieldv[0];
};

int ehclosure_match(EhRt *rt, EhClosure *closure, EhFunction **dst, EhValue ***out, size_t *outc, int argc, EhValue **argv);
EhValue *ehclosure_call(EhRt *rt, EhClosure *closure, int argc, EhValue **argv);
EhValue *ehclosure_tailcall(EhRt *rt, EhClosure *closure, int argc, EhValue **argv);
EhClosure *ehclosure_impl(EhRt *rt, EhClosure *closure, EhValue *impl);
EhValue *ehmod_import(EhRt *rt, EhClosure *closure);

EhValue *ehvar_get(EhRt *rt, EhVar *var);
void ehvar_set(EhRt *rt, EhVar *var, EhValue *inner);

int ehvalue_eq(EhRt *rt, const EhValue *a, const EhValue *b);
int ehnil_eq(EhRt *rt, const EhNil *a, const EhNil *b);
int ehatom_eq(EhRt *rt, const EhAtom *a, const EhAtom *b);
int ehbool_eq(EhRt *rt, const EhBool *a, const EhBool *b);
int ehptr_eq(EhRt *rt, const EhPtr *a, const EhPtr *b);
int ehint_eq(EhRt *rt, const EhInt *a, const EhInt *b);
int ehreal_eq(EhRt *rt, const EhReal *a, const EhReal *b);
int ehchar_eq(EhRt *rt, const EhChar *a, const EhChar *b);
int ehlist_eq(EhRt *rt, const EhList *a, const EhList *b);

int ehvalue_ne(EhRt *rt, const EhValue *a, const EhValue *b);
int ehnil_ne(EhRt *rt, const EhNil *a, const EhNil *b);
int ehatom_ne(EhRt *rt, const EhAtom *a, const EhAtom *b);
int ehbool_ne(EhRt *rt, const EhBool *a, const EhBool *b);
int ehptr_ne(EhRt *rt, const EhPtr *a, const EhPtr *b);
int ehint_ne(EhRt *rt, const EhInt *a, const EhInt *b);
int ehreal_ne(EhRt *rt, const EhReal *a, const EhReal *b);
int ehchar_ne(EhRt *rt, const EhChar *a, const EhChar *b);
int ehlist_ne(EhRt *rt, const EhList *a, const EhList *b);

int ehint_lt(EhRt *rt, const EhInt *a, const EhInt *b);
int ehreal_lt(EhRt *rt, const EhReal *a, const EhReal *b);
int ehint_gt(EhRt *rt, const EhInt *a, const EhInt *b);
int ehreal_gt(EhRt *rt, const EhReal *a, const EhReal *b);
int ehint_le(EhRt *rt, const EhInt *a, const EhInt *b);
int ehreal_le(EhRt *rt, const EhReal *a, const EhReal *b);
int ehint_ge(EhRt *rt, const EhInt *a, const EhInt *b);
int ehreal_ge(EhRt *rt, const EhReal *a, const EhReal *b);

void ehvalue_hash(EhRt *rt, EhHasher *state, const EhValue *a);
void ehnil_hash(EhRt *rt, EhHasher *state, const EhNil *a);
void ehatom_hash(EhRt *rt, EhHasher *state, const EhAtom *a);
void ehbool_hash(EhRt *rt, EhHasher *state, const EhBool *a);
void ehptr_hash(EhRt *rt, EhHasher *state, const EhPtr *a);
void ehint_hash(EhRt *rt, EhHasher *state, const EhInt *a);
void ehreal_hash(EhRt *rt, EhHasher *state, const EhReal *a);
void ehchar_hash(EhRt *rt, EhHasher *state, const EhChar *a);

EhValue *new_nil(EhRt *rt, EhNil *nil);
EhValue *new_atom(EhRt *rt, EhAtom *atom, size_t len, const char *pstr);
EhValue *new_bool(EhRt *rt, EhBool *p, const enum EhBoolTag variant);
EhValue *new_ptr(EhRt *rt, EhPtr *i, void *ptr);
EhValue *new_int(EhRt *rt, EhInt *i, const long value);
EhValue *new_real(EhRt *rt, EhReal *f, const double value);
EhValue *new_char(EhRt *rt, EhChar *ch, const unsigned int value);
EhValue *new_list_empty(EhRt *rt, EhList *empty);
EhValue *new_list_cons(EhRt *rt, EhList *empty, EhValue *head, EhValue *tail);
EhValue *new_bind(EhRt *rt, EhBind *bind, unsigned int type_id, EhClosure *pred);
EhValue *new_destr(EhRt *rt, EhDestr *destr, unsigned int type_id, unsigned int variant, size_t patternc, EhValue **patternv);
EhValue *new_closure(EhRt *rt, EhClosure *closure, int flags, unsigned int implc, EhFunction **implv);
EhValue *new_function(EhRt *rt, EhFunction *function, EhFunc func, EhHtrie *env, size_t paramc, EhValue **paramv, size_t datac, EhValue **data);
EhValue *new_ifunc(EhRt *rt, EhIFunc *function, size_t func, EhHtrie *env, size_t paramc, EhValue **paramv, size_t datac, EhValue **data);
EhValue *new_var(EhRt *rt, EhVar *var, EhValue *inner);
EhValue *new_data(EhRt *rt, EhData *data, unsigned int type, unsigned int variant, size_t fieldc, EhValue **fieldv);

EhValue *ehatom_from_cstr(EhRt *rt, EhAtom *atom, const char *cstr);
EhValue *ehstr_from_pstr(EhRt *rt, EhList *str, size_t len, const char *pstr); 

EhValue *ehlist_concat(EhRt *rt, EhList *a, EhList *b);
EhValue *ehlist_concatv(EhRt *rt, size_t listc, EhList **listv);

EhValue *ehvalue_repr(EhRt *rt, EhValue *value);
EhValue *ehnil_repr(EhRt *rt, EhNil *nil);
EhValue *ehatom_repr(EhRt *rt, EhAtom *atom);
EhValue *ehbool_repr(EhRt *rt, EhBool *p);
EhValue *ehptr_repr(EhRt *rt, EhPtr *i);
EhValue *ehint_repr(EhRt *rt, EhInt *i);
EhValue *ehreal_repr(EhRt *rt, EhReal *x);
EhValue *ehchar_repr(EhRt *rt, EhChar *c);
EhValue *ehlist_repr(EhRt *rt, EhList *l);
EhValue *ehbind_repr(EhRt *rt, EhBind *b);
EhValue *ehdestr_repr(EhRt *rt, EhDestr *d);
EhValue *ehclosure_repr(EhRt *rt, EhClosure *f);
EhValue *ehvar_repr(EhRt *rt, EhVar *f);

EhValue *ehvalue_display(EhRt *rt, EhValue *value);
EhValue *ehnil_display(EhRt *rt, EhNil *nil);
EhValue *ehatom_display(EhRt *rt, EhAtom *atom);
EhValue *ehbool_display(EhRt *rt, EhBool *p);
EhValue *ehptr_display(EhRt *rt, EhPtr *i);
EhValue *ehint_display(EhRt *rt, EhInt *i);
EhValue *ehreal_display(EhRt *rt, EhReal *x);
EhValue *ehchar_display(EhRt *rt, EhChar *c);
EhValue *ehlist_display(EhRt *rt, EhList *l);

void ehstr_write(EhRt *rt, EhList *str);
EhValue *ehchar_read(EhRt *rt);

#endif /* ifndef VALUE_H */
