#ifndef RATIO_H
#define RATIO_H 1

#include "value.h"
#include "rt.h"

typedef struct EhRatio EhRatio;

enum EhRatioSign {
    POS = 0,
    NEG = 1,
};

struct EhRatio {
    VALUE_HEADER;
    enum EhRatioSign sign;
    unsigned long p;
    unsigned long q;
};

EhValue *new_ratio(EhRt *rt, EhRatio *ratio, enum EhRatioSign sign, unsigned long p, unsigned long q);
EhValue *ehratio_neg(EhRt *rt, EhRatio *lhs);
EhValue *ehratio_add(EhRt *rt, EhRatio *lhs, EhRatio *rhs);
EhValue *ehratio_sub(EhRt *rt, EhRatio *lhs, EhRatio *rhs);
EhValue *ehratio_mul(EhRt *rt, EhRatio *lhs, EhRatio *rhs);
EhValue *ehratio_div(EhRt *rt, EhRatio *lhs, EhRatio *rhs);

int ehratio_eq(EhRt *rt, const EhRatio *a, const EhRatio *b);
int ehratio_ne(EhRt *rt, const EhRatio *a, const EhRatio *b);
int ehratio_lt(EhRt *rt, const EhRatio *a, const EhRatio *b);
int ehratio_gt(EhRt *rt, const EhRatio *a, const EhRatio *b);
int ehratio_le(EhRt *rt, const EhRatio *a, const EhRatio *b);
int ehratio_ge(EhRt *rt, const EhRatio *a, const EhRatio *b);
void ehratio_hash(EhRt *rt, EhHasher *state, const EhRatio *a);

EhValue *ehratio_repr(EhRt *rt, EhRatio *i);
EhValue *ehratio_display(EhRt *rt, EhRatio *i);

#endif /* RATIO_H */
