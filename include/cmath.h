#ifndef MATH_H
#define MATH_H 1

#include "rt.h"

void init_lib_cmath(EhRt *rt);

EhValue *cmath_sin(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_tan(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_acos(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_asin(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_atan(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_atan2(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_cosh(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_sinh(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_tanh(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_acosh(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_asinh(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_atanh(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_exp(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_frexp(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_ldexp(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_log(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_log10(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_modf(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_exp2(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_expm1(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_ilogb(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_log1p(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_log2(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_logb(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_scalbn(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_scalbln(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_pow(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_sqrt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_cbrt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_hypot(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_erf(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_erfc(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_tgamma(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_lgamma(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_ceil(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_floor(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_fmod(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_trunc(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_round(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_lround(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_rint(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_lrint(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_nearbyint(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_remainder(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_remquo(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_copysign(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_nan(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_nextafter(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_nexttoward(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_fdim(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_fmax(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_fmin(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_fabs(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_abs(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_fma(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *cmath_fpclassify(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_isfinite(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_isinf(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_isnan(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_isnormal(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *cmath_signbit(EhRt *rt, EhValue **data, EhValue **argv);

#endif /* MATH_H */
