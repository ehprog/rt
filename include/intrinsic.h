#ifndef INTRINSIC_H
#define INTRINSIC_H 1

#include "value.h"
#include "rt.h"

EhValue *ehintr_nil_eq(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_bool_eq(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_eq(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_eq(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_eq(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_char_eq(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_nil_ne(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_bool_ne(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_ne(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_ne(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_ne(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_char_ne(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_int_le(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_le(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_le(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_ge(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_ge(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_ge(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_lt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_lt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_lt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_gt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_gt(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_gt(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_nil_hash(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_bool_hash(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_hash(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_hash(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_hash(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_char_hash(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_int_add(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_add(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_add(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_sub(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_sub(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_sub(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_mul(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_mul(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_mul(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_div(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_div(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_div(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_mod(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_list_head(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_list_tail(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_var_get(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_var_update(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_new_nil(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_atom_id(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_bool_true(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_bool_false(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_int_id(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_ratio_ii(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_real_id(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_char_id(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_list_empty(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_list_cons(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_new_var_any(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_nil_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_atom_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_bool_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_char_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_list_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_bind_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_destr_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_closure_repr(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_var_repr(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_nil_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_atom_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_bool_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_char_display(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_list_display(EhRt *rt, EhValue **data, EhValue **argv);

EhValue *ehintr_int_to_ratio(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_to_real(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_int_to_char(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_to_int(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_ratio_to_real(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_to_int(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_real_to_ratio(EhRt *rt, EhValue **data, EhValue **argv);
EhValue *ehintr_char_to_int(EhRt *rt, EhValue **data, EhValue **argv);

#endif /* INTRINSIC_H */
