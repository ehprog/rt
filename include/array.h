#ifndef ARRAY_H
#define ARRAY_H 1

#include <stdlib.h>
#include "rt.h"

#define EHARRAY_CAPACITY 64

typedef struct {
    unsigned char *buffer;
    size_t len;
    size_t elem_size;
    size_t cap;
} EhArray;

typedef struct {
    unsigned char *buffer;
    size_t len;
    size_t elem_size;
} EhSlice;

typedef struct {
    unsigned char *buffer;
    size_t len;
    size_t elem_size;
} EhSliceIter;

EhArray *new_array(EhRt *rt, EhArray *dst, const size_t cap, const size_t elem_size);
void del_array(EhRt *rt, EhArray *dst);
void *eharray_index(EhRt *rt, EhArray *array, const size_t idx);
EhSlice *eharray_slice(EhRt *rt, EhArray *array, EhSlice *dst, const size_t from, const size_t to);
EhSliceIter *eharray_iter(EhRt *rt, EhArray *array, EhSliceIter *dst);
void eharray_get(EhRt *rt, const EhArray *array, void *dst, const size_t idx);
void eharray_set(EhRt *rt, EhArray *array, const void *src, const size_t idx);
void eharray_push(EhRt *rt, EhArray *array, const void *elem);
void eharray_pop(EhRt *rt, EhArray *array, void *dst);
void eharray_shrink(EhRt *rt, EhArray *array);

void *ehslice_index(EhRt *rt, EhSlice *slice, const size_t idx);
EhSlice *ehslice_slice(EhRt *rt, EhSlice *slice, EhSlice *dst, const size_t from, const size_t to);
EhSliceIter *ehslice_iter(EhRt *rt, EhSlice *slice, EhSliceIter *dst);
void ehslice_get(EhRt *rt, const EhSlice *slice, void *dst, const size_t idx);
void ehslice_set(EhRt *rt, EhSlice *slice, const void *src, const size_t idx);

int ehsliceiter_next(EhRt *rt, EhSliceIter *iter, void *dst);

#endif /* ifndef ARRAY_H */
