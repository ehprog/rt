include config.mk

C_SRC:=$(wildcard $(SRCDIR)/*.c)
ASM_SRC:=$(wildcard $(SRCDIR)/$(ARCH)-$(OS)-$(USERSPACE)/*.asm)

SRC+=$(C_SRC) $(ASM_SRC)
OBJ+=$(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.c.o,$(C_SRC)) \
	$(patsubst $(SRCDIR)/$(ARCH)-$(OS)-$(USERSPACE)/%.asm,$(OBJDIR)/%.asm.o,$(ASM_SRC))
BIN=$(BUILD)/$(BINNAME).$(VERSION)
LIB+=
ROOT?=$(HOME)/.local

.PHONY: all install clean mrproper

all: $(OBJDIR) $(BUILD) $(BIN)

install: all
	$(RUN) mkdir -p root/include/ehrt
	$(RUN) mkdir -p root/$(LIBDIR)
	$(RUN) cp -r include/* root/include/ehrt/
	$(RUN) cp -r $(BIN) root/$(LIBDIR)/
	$(RUN) cp -r root/* $(ROOT)/
	$(RUN) pushd $(ROOT)/$(LIBDIR); ln -fs $(BINNAME).$(VERSION) $(BINNAME).$(VERSION_SHORT); ln -fs $(BINNAME).$(VERSION_SHORT) $(BINNAME).$(VERSION_SHORTER); ln -fs $(BINNAME).$(VERSION_SHORTER) $(BINNAME); popd

$(OBJDIR):
	$(RUN) mkdir -p $(OBJDIR)

$(BUILD):
	$(RUN) mkdir -p $(BUILD)

$(OBJDIR)/%.c.o: $(SRCDIR)/%.c
	$(RUN) $(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $^

$(OBJDIR)/%.asm.o: $(SRCDIR)/$(ARCH)-$(OS)-$(USERSPACE)/%.asm
	$(RUN) $(AS) $(ASFLAGS) $(INCLUDE) -o $@ $^

$(BIN): $(OBJ) $(LIB)
	$(RUN) $(LD) $(LDFLAGS) -o $@ $^

clean:
	$(RUN) rm -rf target root

mrproper: clean
	$(RUN) rm -rf $(BINNAME)
